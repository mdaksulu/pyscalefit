#!/Users/mda/opt/anaconda3/bin/python
# -*- coding: utf-8 -*-

import json

import george
import matplotlib.pyplot as plt
import numpy as np
import pymultinest.analyse as analyse
from george import kernels
from getdist import MCSamples, plots
from pymultinest.solve import solve

import misc
import plot
from SF_flux import simulation_templates
from file_io import FileIO
from kde_sampler import sample_from_posterior
from afterglow_data.extinction import application

TINY_LIKELIHOOD = -1e300
HUGE_CHI2 = 1e300


def prior(cube):
    new_cube = np.zeros(len(cube))
    if not fio.conf_data['fit']['gp']['use_gp']:
        new_cube[:fio.len_free_scalefit_params] = fio.convert_cube_to_scalefit_param(
            cube[:fio.len_free_scalefit_params])
        if fio.conf_data['fit']['host']['fit_persistent']:
            new_cube[fio.len_free_scalefit_params:
                     fio.len_free_scalefit_params +
                     fio.len_free_persistent_params] = fio.convert_cube_to_persistent_param(
                cube[fio.len_free_scalefit_params:
                     fio.len_free_scalefit_params
                     + fio.len_free_persistent_params])
        return new_cube
    else:
        new_cube[:fio.len_free_hyper_params] = fio.convert_cube_to_hyper_param(cube[:fio.len_free_hyper_params])
        new_cube[
        fio.len_free_hyper_params:fio.len_free_hyper_params
                                  + fio.len_free_scalefit_params] = fio.convert_cube_to_scalefit_param(
            cube[fio.len_free_hyper_params:fio.len_free_hyper_params + fio.len_free_scalefit_params])
        if fio.conf_data['fit']['host']['fit_persistent']:
            new_cube[fio.len_free_hyper_params + fio.len_free_scalefit_params:
                     fio.len_free_hyper_params + fio.len_free_scalefit_params +
                     fio.len_free_persistent_params] = fio.convert_cube_to_persistent_param(
                cube[fio.len_free_hyper_params + fio.len_free_scalefit_params:
                     fio.len_free_hyper_params + fio.len_free_scalefit_params
                     + fio.len_free_persistent_params])
        return new_cube


def loglikelihood(cube):
    scalefit_parameters = np.zeros(scalefit.no_pars)
    if not fio.conf_data['fit']['gp']['use_gp']:
        scalefit_parameters[fio.free_scalefit_params_mask] = cube[:fio.len_free_scalefit_params]
        scalefit_parameters[fio.fixed_scalefit_params_mask] = fio.fixed_scalefit_params
        scalefit_parameters = fio.order_scalefit_parameters(scalefit_parameters)
        # epsilon_E = 10 ** scalefit_parameters[scalefit.index_log10_eps_e_bar_] * (
        #         scalefit_parameters[scalefit.index_p_] - 1) / (
        #                     scalefit_parameters[scalefit.index_p_] - 2)
        # if epsilon_E > 0.5:
        #     return TINY_LIKELIHOOD
        f_nu = scalefit.Fnu(scalefit_parameters, fio.obs_time, fio.obs_freq)
        if np.any(np.isinf(f_nu)) or np.any(np.isnan(f_nu)):
            return TINY_LIKELIHOOD
        if fio.conf_data['fit']['host']['fit_persistent']:
            persistent_params = np.zeros(fio.len_free_persistent_params)
            persistent_params[fio.free_persistent_params_mask] = cube[fio.len_free_scalefit_params:
                                                                      fio.len_free_scalefit_params +
                                                                      fio.len_free_persistent_params]
            for b, band in enumerate(fio.persistent_bands):
                b_mask = misc.in_range(fio.obs_freq, band)
                if fio.conf_data['fit']['host']['log10_flux']:
                    f_nu[b_mask] = f_nu[b_mask] + 10 ** persistent_params[b]
                else:
                    f_nu[b_mask] = f_nu[b_mask] + persistent_params[b]
        chi2 = np.sum((fio.obs_flux - f_nu) ** 2 / fio.obs_error ** 2)
        return -1 * chi2
    else:
        hyper_params = np.zeros(len(fio.hyper_param_names))
        hyper_params[fio.free_hyper_params_mask] = cube[:fio.len_free_hyper_params]
        hyper_params[fio.fixed_hyper_params_mask] = fio.fixed_hyper_params
        a1, a2, l1, l2, white_noise = hyper_params
        scalefit_parameters[
            fio.free_scalefit_params_mask] = cube[fio.len_free_hyper_params:
                                                  fio.len_free_scalefit_params + fio.len_free_hyper_params]
        scalefit_parameters[fio.fixed_scalefit_params_mask] = fio.fixed_scalefit_params
        scalefit_parameters = fio.order_scalefit_parameters(scalefit_parameters)
        # epsilon_E = 10 ** scalefit_parameters[scalefit.index_log10_eps_e_bar_] * (
        #         scalefit_parameters[scalefit.index_p_] - 1) / (
        #                     scalefit_parameters[scalefit.index_p_] - 2)
        # if epsilon_E > 0.5:
        #     return TINY_LIKELIHOOD
        k = 10 ** a1 * kernels.ExpSquaredKernel([10 ** l1, 10 ** l2], ndim=2)
        gp = george.GP(k)
        f_nu = scalefit.Fnu(scalefit_parameters, fio.obs_time, fio.obs_freq)
        if np.any(np.isinf(f_nu)) or np.any(np.isnan(f_nu)):
            return TINY_LIKELIHOOD
        # Fit for extinction
        if fio.conf_data['fit']['host']['fit_persistent']:
            persistent_params = np.zeros(fio.len_free_persistent_params)
            persistent_params[fio.free_persistent_params_mask] = cube[fio.len_free_hyper_params +
                                                                      fio.len_free_scalefit_params:
                                                                      fio.len_free_hyper_params +
                                                                      fio.len_free_scalefit_params +
                                                                      fio.len_free_persistent_params]
            for b, band in enumerate(fio.persistent_bands):
                b_mask = misc.in_range(fio.obs_freq, band)
                if fio.conf_data['fit']['host']['log10_flux']:
                    f_nu[b_mask] = f_nu[b_mask] + 10 ** persistent_params[b]
                else:
                    f_nu[b_mask] = f_nu[b_mask] + persistent_params[b]
        if fio.conf_data['fit']['gp']['log_time_coord']:
            x1 = fio.log_obs_time
        else:
            x1 = fio.obs_time
        if fio.conf_data['fit']['gp']['log_freq_coord']:
            x2 = fio.log_obs_freq
        else:
            x2 = fio.obs_freq
        x_2d = np.column_stack((x1, x2))
        try:
            if fio.conf_data['fit']['gp']['use_errorbars']:
                if fio.conf_data['fit']['gp']['log_flux']:
                    gp.compute(x_2d, fio.log_obs_error * np.exp(white_noise))
                    # gp.compute(x_2d, (fio.obs_error / fio.obs_flux) * np.exp(white_noise))
                else:
                    gp.compute(x_2d, fio.obs_error * np.exp(white_noise))
            else:
                gp.compute(x_2d, np.exp(white_noise))
        except np.linalg.LinAlgError:
            return TINY_LIKELIHOOD
        if fio.conf_data['fit']['gp']['log_flux']:
            f_nu[f_nu == 0] = 1e-30
            y = fio.log_obs_flux - np.log(f_nu)
        else:
            y = fio.obs_flux - f_nu
        if np.any(np.isinf(y)) or np.any(np.isnan(y)):
            return TINY_LIKELIHOOD
        try:
            likelihood = gp.log_likelihood(y, quiet=True)
        except np.linalg.LinAlgError:
            return TINY_LIKELIHOOD
        return likelihood


def chi2(cube):
    scalefit_parameters = np.zeros(scalefit.no_pars)
    scalefit_parameters[fio.free_scalefit_params_mask] = cube[:fio.len_free_scalefit_params]
    scalefit_parameters[fio.fixed_scalefit_params_mask] = fio.fixed_scalefit_params
    scalefit_parameters = fio.order_scalefit_parameters(scalefit_parameters)
    epsilon_E = 10 ** scalefit_parameters[scalefit.index_log10_eps_e_bar_] * (
            scalefit_parameters[scalefit.index_p_] - 1) / (
                        scalefit_parameters[scalefit.index_p_] - 2)
    if epsilon_E > 0.5:
        return HUGE_CHI2
    f_nu = scalefit.Fnu(scalefit_parameters, fio.obs_time, fio.obs_freq)
    if np.any(np.isinf(f_nu)) or np.any(np.isnan(f_nu)):
        return HUGE_CHI2
    if fio.conf_data['fit']['host']['fit_persistent']:
        persistent_params = np.zeros(fio.len_free_persistent_params)
        persistent_params[fio.free_persistent_params_mask] = cube[fio.len_free_scalefit_params:
                                                                  fio.len_free_scalefit_params +
                                                                  fio.len_free_persistent_params]
        for b, band in enumerate(fio.persistent_bands):
            b_mask = misc.in_range(fio.obs_freq, band)
            if fio.conf_data['fit']['host']['log10_flux']:
                f_nu[b_mask] = f_nu[b_mask] + 10 ** persistent_params[b]
            else:
                f_nu[b_mask] = f_nu[b_mask] + persistent_params[b]
    chi2 = np.sum((fio.obs_flux - f_nu) ** 2 / fio.obs_error ** 2)
    return chi2


fio = FileIO()
scalefit = simulation_templates(fio.conf_data['scalefit']['tabledir'])
if fio.conf_data['scalefit']['smooth'] == 1:
    scalefit.smooth = True
elif fio.conf_data['scalefit']['smooth'] == 0:
    scalefit.smooth = False
print('Total number of free parameters:', fio.len_free_params)
print('Total number of free hyper parameters:', fio.len_free_hyper_params)
print('Total number of free scalefit parameters:', fio.len_free_scalefit_params)
print('Total number of free persistent parameters:', fio.len_free_persistent_params)
print('Total number of free extinction parameters:', fio.len_free_extinction_params)

workspace_path = fio.conf_data['IO']['outdir'] + '/' + fio.conf_data['IO']['prefix']
grb_name = 'GRB ' + fio.conf_data['IO']['prefix'][:-1]
print(grb_name)
mn_param_names = fio.free_param_names()
mn_n_params = fio.len_free_params

if fio.conf_data['scalefit']['mode'] == 1:
    result = solve(LogLikelihood=loglikelihood,
                   Prior=prior,
                   n_dims=mn_n_params,
                   outputfiles_basename=workspace_path,
                   verbose=True,
                   evidence_tolerance=fio.conf_data['multinest']['evidence_tolerance'],
                   n_live_points=fio.conf_data['multinest']['n_live_points'],
                   const_efficiency_mode=fio.conf_data['multinest']['const_efficiency_mode'],
                   importance_nested_sampling=fio.conf_data['multinest']['importance_nested_sampling'],
                   n_iter_before_update=10,
                   n_clustering_params=mn_n_params)

    print()
    print('evidence: %(logZ).1f +- %(logZerr).1f' % result)
    print()
    print('parameter values:')

analyzer = analyse.Analyzer(mn_n_params, outputfiles_basename=workspace_path)
stats = analyzer.get_stats()
bestfit_stat = analyzer.get_best_fit()
with open(workspace_path + 'stats.json', 'w') as fp:
    json.dump(stats, fp, sort_keys=True, indent=4)
with open(workspace_path + 'bestfit.json', 'w') as fp:
    json.dump(bestfit_stat, fp, sort_keys=True, indent=4)
equal_weighted_params = analyzer.get_equal_weighted_posterior()
dist = MCSamples(samples=equal_weighted_params[:, :mn_n_params], names=mn_param_names,
                 labels=fio.fancy_free_param_names())
dist_model = MCSamples(samples=equal_weighted_params[:, fio.len_free_hyper_params:mn_n_params],
                       names=mn_param_names[fio.len_free_hyper_params:],
                       labels=fio.fancy_free_param_names()[fio.len_free_hyper_params:])
res_table = dist.getTable()
res_table.write(workspace_path + 'res_table.txt')
defaultSettings = plots.GetDistPlotSettings()
defaultSettings.rcSizes(axes_fontsize=12, lab_fontsize=12)
g = plots.getSubplotPlotter(width_inch=10)
g.settings.rcSizes(axes_fontsize=6)
g.settings.num_plot_contours = 5
if fio.conf_data['fit']['gp']['use_gp']:
    g.triangle_plot(dist, filled=True)
    plt.savefig(workspace_path + 'marg_getdist.pdf')
g.triangle_plot(dist_model, filled=True)
plt.savefig(workspace_path + 'marg-model_getdist.pdf')

bestfit_scalefit_params = np.zeros(scalefit.no_pars)
bestfit_scalefit_params[fio.free_scalefit_params_mask] = bestfit_stat['parameters'][
                                                         fio.len_free_hyper_params:fio.len_free_hyper_params +
                                                                                   fio.len_free_scalefit_params]
bestfit_scalefit_params[fio.fixed_scalefit_params_mask] = fio.fixed_scalefit_params
f_nu_scalefit = scalefit.Fnu(fio.order_scalefit_parameters(bestfit_scalefit_params), fio.obs_time, fio.obs_freq)
if fio.conf_data['fit']['host']['fit_persistent']:
    best_persistent_parameters = bestfit_stat['parameters'][fio.len_free_hyper_params +
                                                            fio.len_free_scalefit_params:
                                                            fio.len_free_hyper_params +
                                                            fio.len_free_scalefit_params +
                                                            fio.len_free_persistent_params]
    for b, p_band in enumerate(fio.persistent_bands):
        b_mask = misc.in_range(fio.obs_freq, p_band)
        if fio.conf_data['fit']['host']['log10_flux']:
            f_nu_scalefit[b_mask] = f_nu_scalefit[b_mask] + 10 ** best_persistent_parameters[b]
        else:
            f_nu_scalefit[b_mask] = f_nu_scalefit[b_mask] + best_persistent_parameters[b]
bestfit_chi2 = np.sum((fio.obs_flux - f_nu_scalefit) ** 2 / fio.obs_error ** 2)
bestfit_dict = {}
for i, name in enumerate(fio.scalefit_param_names):
    bestfit_dict[name] = bestfit_scalefit_params[i]
bestfit_dict['chi-squared'] = bestfit_chi2
bestfit_dict['chi-squared-reduced'] = bestfit_chi2 / (len(fio.obs_flux) - mn_n_params)
bestfit_dict['chi-squared-reduced-model'] = bestfit_chi2 / (len(fio.obs_flux) - fio.len_free_scalefit_params)
with open(workspace_path + 'bestfit-detailed.json', 'w') as fp:
    json.dump(bestfit_dict, fp, sort_keys=True, indent=4)

if not fio.conf_data['fit']['host']['fit_persistent']:
    plot.plot_scalefit(fio.conf_data['plotting']['obs_data'], scalefit,
                       fio.order_scalefit_parameters(bestfit_scalefit_params),
                       factor_val=5,
                       figsize=(4, 8),
                       markersize=3)
else:
    plot.plot_scalefit(fio.conf_data['plotting']['obs_data'], scalefit,
                       fio.order_scalefit_parameters(bestfit_scalefit_params),
                       factor_val=5,
                       figsize=(4, 8),
                       markersize=3, persistent_bands=fio.persistent_bands,
                       persistent_parameters=best_persistent_parameters,
                       log10_flux=fio.conf_data['fit']['host']['log10_flux'])
plt.tight_layout()
plt.savefig(workspace_path + 'lc_best.pdf')

if fio.conf_data['fit']['gp']['use_gp']:
    if not fio.conf_data['fit']['host']['fit_persistent']:
        bestfit_hyper_params = np.zeros(len(fio.hyper_param_names))
        bestfit_hyper_params[fio.free_hyper_params_mask] = bestfit_stat['parameters'][:fio.len_free_hyper_params]
        bestfit_hyper_params[fio.fixed_hyper_params_mask] = fio.fixed_hyper_params
        bestfit_scalefit_params[fio.fixed_scalefit_params_mask] = fio.fixed_scalefit_params
        plot.plot_scalefit_gp(fio.conf_data['plotting']['obs_data'], scalefit_object=scalefit,
                              scalefit_parameters=fio.order_scalefit_parameters(bestfit_scalefit_params),
                              hyper_parameters=bestfit_hyper_params, fio_object=fio, factor_val=5,
                              figsize=(4, 8), markersize=3, plt_boxfit=fio.conf_data['plotting']['plt_boxfit'],
                              box_lc_output_prefix=workspace_path + 'box_lc_best_')
        plt.tight_layout()
        plt.savefig(workspace_path + 'lc_gp_best.pdf')
    else:
        bestfit_hyper_params = np.zeros(len(fio.hyper_param_names))
        bestfit_hyper_params[fio.free_hyper_params_mask] = bestfit_stat['parameters'][:fio.len_free_hyper_params]
        bestfit_hyper_params[fio.fixed_hyper_params_mask] = fio.fixed_hyper_params
        bestfit_scalefit_params[fio.fixed_scalefit_params_mask] = fio.fixed_scalefit_params
        plot.plot_scalefit_gp(fio.conf_data['plotting']['obs_data'], scalefit_object=scalefit,
                              scalefit_parameters=fio.order_scalefit_parameters(bestfit_scalefit_params),
                              hyper_parameters=bestfit_hyper_params, fio_object=fio, factor_val=5,
                              figsize=(4, 8), markersize=3, persistent_bands=fio.persistent_bands,
                              persistent_parameters=best_persistent_parameters,
                              log10_flux=fio.conf_data['fit']['host']['log10_flux'],
                              plt_boxfit=fio.conf_data['plotting']['plt_boxfit'],
                              box_lc_output_prefix=workspace_path + 'box_lc_best_')
        plt.tight_layout()
        plt.savefig(workspace_path + 'lc_gp_best.pdf')

marge_stat = dist.getMargeStats()
print(marge_stat)
mean_scalefit_parameters = np.zeros(scalefit.no_pars)
for i, name in enumerate(fio.scalefit_param_names[fio.free_scalefit_params_mask]):
    p = marge_stat.parsWithNames(name)
    mean_scalefit_parameters[fio.scalefit_param_names == name] = p[0].mean
mean_scalefit_parameters[fio.fixed_scalefit_params_mask] = fio.fixed_scalefit_params
print(mean_scalefit_parameters)
if fio.conf_data['fit']['host']['fit_persistent']:
    mean_persistent_parameters = np.zeros(len(fio.persistent_param_names))
    for i, name in enumerate(fio.persistent_param_names):
        p = marge_stat.parsWithNames(name)
        mean_persistent_parameters[i] = p[0].mean
    print(mean_persistent_parameters)

f_nu_scalefit = scalefit.Fnu(fio.order_scalefit_parameters(mean_scalefit_parameters), fio.obs_time, fio.obs_freq)
if fio.conf_data['fit']['host']['fit_persistent']:
    for b, p_band in enumerate(fio.persistent_bands):
        b_mask = misc.in_range(fio.obs_freq, p_band)
        if fio.conf_data['fit']['host']['log10_flux']:
            f_nu_scalefit[b_mask] = f_nu_scalefit[b_mask] + 10 ** mean_persistent_parameters[b]
        else:
            f_nu_scalefit[b_mask] = f_nu_scalefit[b_mask] + mean_persistent_parameters[b]
mean_chi2 = np.sum((fio.obs_flux - f_nu_scalefit) ** 2 / fio.obs_error ** 2)
mean_dict = {}
for i, name in enumerate(fio.scalefit_param_names):
    mean_dict[name] = mean_scalefit_parameters[i]
mean_dict['chi-squared'] = mean_chi2
mean_dict['chi-squared-reduced'] = mean_chi2 / (len(fio.obs_flux) - mn_n_params)
mean_dict['chi-squared-reduced-model'] = mean_chi2 / (len(fio.obs_flux) - fio.len_free_scalefit_params)
with open(workspace_path + 'mean-detailed.json', 'w') as fp:
    json.dump(mean_dict, fp, sort_keys=True, indent=4)
if not fio.conf_data['fit']['host']['fit_persistent']:
    plot.plot_scalefit(fio.conf_data['plotting']['obs_data'], scalefit,
                       fio.order_scalefit_parameters(mean_scalefit_parameters),
                       factor_val=5,
                       figsize=(4, 8),
                       markersize=3)
else:
    plot.plot_scalefit(fio.conf_data['plotting']['obs_data'], scalefit,
                       fio.order_scalefit_parameters(mean_scalefit_parameters),
                       factor_val=5,
                       figsize=(4, 8),
                       markersize=3, persistent_bands=fio.persistent_bands,
                       persistent_parameters=mean_persistent_parameters,
                       log10_flux=fio.conf_data['fit']['host']['log10_flux'])
plt.tight_layout()
plt.savefig(workspace_path + 'lc_mean.pdf')

if fio.conf_data['fit']['gp']['use_gp']:
    mean_hyper_parameters = np.zeros(len(fio.hyper_param_names))
    for i, name in enumerate(fio.hyper_param_names[fio.free_hyper_params_mask]):
        p = marge_stat.parsWithNames(name)
        mean_hyper_parameters[fio.hyper_param_names == name] = p[0].mean
    mean_hyper_parameters[fio.fixed_hyper_params_mask] = fio.fixed_hyper_params
    print(mean_hyper_parameters)
    if not fio.conf_data['fit']['host']['fit_persistent']:
        plot.plot_scalefit_gp(fio.conf_data['plotting']['obs_data'], scalefit_object=scalefit,
                              scalefit_parameters=fio.order_scalefit_parameters(mean_scalefit_parameters),
                              hyper_parameters=mean_hyper_parameters, fio_object=fio, factor_val=5,
                              figsize=(4, 8), markersize=3, plt_boxfit=fio.conf_data['plotting']['plt_boxfit'],
                              box_lc_output_prefix=workspace_path + 'box_lc_mean_')
        plt.tight_layout()
        plt.savefig(workspace_path + 'lc_gp_mean.pdf')
    else:
        plot.plot_scalefit_gp(fio.conf_data['plotting']['obs_data'], scalefit_object=scalefit,
                              scalefit_parameters=fio.order_scalefit_parameters(mean_scalefit_parameters),
                              hyper_parameters=bestfit_hyper_params, fio_object=fio, factor_val=5,
                              figsize=(4, 8), markersize=3, persistent_bands=fio.persistent_bands,
                              persistent_parameters=mean_persistent_parameters,
                              log10_flux=fio.conf_data['fit']['host']['log10_flux'],
                              plt_boxfit=fio.conf_data['plotting']['plt_boxfit'],
                              box_lc_output_prefix=workspace_path + 'box_lc_mean_')
        plt.tight_layout()
        plt.savefig(workspace_path + 'lc_gp_mean.pdf')

# print(equal_weighted_params[:, fio.len_free_hyper_params:mn_n_params])
n_sample = 100
# data = analyzer.get_data()[:, 2:]
# weights = analyzer.get_data()[:, 0]
# weight_mask = (weights > 1e-4)
# data = data[weight_mask]
# weights = weights[weight_mask]
resampled_mn_params = np.array(
    sample_from_posterior(np.array(equal_weighted_params[:, fio.len_free_hyper_params:mn_n_params]).T,
                          n=n_sample)).T
# resampled_mn_params = np.array(
#     sample_from_posterior(np.array(data[:, fio.len_free_hyper_params:mn_n_params]).T,
#                           n=n_sample, weights=weights)).T
resampled_scalefit_params = np.zeros((n_sample, scalefit.no_pars))
for i in range(n_sample):
    resampled_scalefit_params[i][fio.free_scalefit_params_mask] = resampled_mn_params[i][:fio.len_free_scalefit_params]
    resampled_scalefit_params[i][fio.fixed_scalefit_params_mask] = fio.fixed_scalefit_params
    resampled_scalefit_params[i] = fio.order_scalefit_parameters(resampled_scalefit_params[i])
# print(resampled_scalefit_params)
if fio.conf_data['fit']['host']['fit_persistent']:
    resampled_persistent_params = np.zeros((n_sample, fio.len_free_persistent_params))
    for i in range(n_sample):
        resampled_persistent_params[i] = resampled_mn_params[i][fio.len_free_scalefit_params:]
    plot.plot_scalefit_multiple(fio.conf_data['plotting']['obs_data'], scalefit,
                                resampled_scalefit_params,
                                factor_val=5,
                                figsize=(4, 8),
                                markersize=3, persistent_bands=fio.persistent_bands,
                                persistent_parameters=resampled_persistent_params,
                                log10_flux=fio.conf_data['fit']['host']['log10_flux'],
                                grb_name=grb_name)
    plt.tight_layout()
    plt.savefig(workspace_path + 'lc_posterior.pdf')
else:
    plot.plot_scalefit_multiple(fio.conf_data['plotting']['obs_data'], scalefit,
                                resampled_scalefit_params,
                                factor_val=5,
                                figsize=(4, 8),
                                markersize=3,
                                grb_name=grb_name)
    plt.tight_layout()
    plt.savefig(workspace_path + 'lc_posterior.pdf')

# fine_tunning = True
# n_new_dist = 1000
# n_iter = 100000
# current_iter = 0
# if fine_tunning:
#     population = np.array(
#         sample_from_posterior(np.array(equal_weighted_params[:, fio.len_free_hyper_params:mn_n_params]).T,
#                               n=n_new_dist)).T
#     fitness = np.zeros(n_new_dist)
#     for i, param in enumerate(population):
#         fitness[i] = chi2(param)
#     dchi = HUGE_CHI2
#     while (current_iter < n_iter) and (dchi > 1.0):
#         best_index = np.argmin(fitness)
#         worst_index = np.argmax(fitness)
#         best_chi2 = fitness[best_index]
#         best_param = population[best_index]
#         worst_chi2 = fitness[worst_index]
#         worst_param = population[worst_index]
#         dchi = worst_chi2 - best_chi2
#         print('Best param :', best_param)
#         print('Best chi2  :', best_chi2)
#         print('Worst chi2 :', worst_chi2)
#         print('dchi       :', dchi)
#         new_param = np.array(
#             sample_from_posterior(np.array(equal_weighted_params[:, fio.len_free_hyper_params:mn_n_params]).T,
#                                   n=1)).T
#         # new_param = np.array(
#         #     sample_from_posterior(population.T,
#         #                           n=1)).T
#         new_chi2 = chi2(new_param[0])
#         if new_chi2 < worst_chi2:
#             population[worst_index] = new_param[0]
#             fitness[worst_index] = new_chi2
#         current_iter = current_iter + 1
#
# dist_fine_tuned = MCSamples(samples=population,
#                             names=mn_param_names[fio.len_free_hyper_params:],
#                             labels=fio.fancy_free_param_names()[fio.len_free_hyper_params:])
# res_table_fine = dist_fine_tuned.getTable()
# res_table_fine.write(workspace_path + 'fine-res_table.txt')
# defaultSettings = plots.GetDistPlotSettings()
# defaultSettings.rcSizes(axes_fontsize=12, lab_fontsize=12)
# g = plots.getSubplotPlotter(width_inch=10)
# g.settings.rcSizes(axes_fontsize=6)
# g.settings.num_plot_contours = 5
# g.triangle_plot(dist_fine_tuned, filled=True)
# plt.savefig(workspace_path + 'marg-fine-model_getdist.pdf')
# g.triangle_plot([dist_fine_tuned, dist_model], filled=True, legend_labels=['Finetuned', 'Multinest'])
# plt.savefig(workspace_path + 'marg-fine-mn-model_getdist.pdf')
#
# pop_resampled = np.array(
#             sample_from_posterior(population.T,
#                                   n=n_sample)).T
# resampled_scalefit_params = np.zeros((n_sample, scalefit.no_pars))
# for i in range(n_sample):
#     resampled_scalefit_params[i][fio.free_scalefit_params_mask] = pop_resampled[i][:fio.len_free_scalefit_params]
#     resampled_scalefit_params[i][fio.fixed_scalefit_params_mask] = fio.fixed_scalefit_params
#     resampled_scalefit_params[i] = fio.order_scalefit_parameters(resampled_scalefit_params[i])
#
# if fio.conf_data['fit']['host']['fit_persistent']:
#     resampled_persistent_params = np.zeros((n_sample, fio.len_free_persistent_params))
#     for i in range(n_sample):
#         resampled_persistent_params[i] = pop_resampled[i][fio.len_free_scalefit_params:]
#     plot.plot_scalefit_multiple(fio.conf_data['plotting']['obs_data'], scalefit,
#                                 resampled_scalefit_params,
#                                 factor_val=5,
#                                 figsize=(4, 8),
#                                 markersize=3, persistent_bands=fio.persistent_bands,
#                                 persistent_parameters=resampled_persistent_params,
#                                 log10_flux=fio.conf_data['fit']['host']['log10_flux'])
#     plt.tight_layout()
#     plt.savefig(workspace_path + 'lc_posterior-fine.pdf')
# else:
#     plot.plot_scalefit_multiple(fio.conf_data['plotting']['obs_data'], scalefit,
#                                 resampled_scalefit_params,
#                                 factor_val=5,
#                                 figsize=(4, 8),
#                                 markersize=3)
#     plt.tight_layout()
#     plt.savefig(workspace_path + 'lc_posterior-fine.pdf')
