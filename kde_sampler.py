from scipy.stats import gaussian_kde


def kde_from_posterior(posterior):
    return gaussian_kde(posterior)


def sample_from_posterior(posterior, n=1, weights=None):
    if weights is None:
        kde = gaussian_kde(posterior)
    else:
        kde = gaussian_kde(posterior, weights=weights)
    return kde.resample(n)
