import numpy as np


def group(unique, accuracy=0.03):
    sorted_unique = sorted(unique)
    result = []
    for i in range(len(sorted_unique)):
        exists = False
        for j in range(len(result)):
            if accuracy < 1.0:
                if sorted_unique[i] * (1 - accuracy) <= result[j] <= sorted_unique[i] * (1 + accuracy):
                    exists = True
            else:
                if sorted_unique[i] / accuracy <= result[j] <= sorted_unique[i] * accuracy:
                    exists = True
        if not exists:
            result.append(sorted_unique[i])
    return np.array(result)


def get_mask(df, val, accuracy=0.03, key='Freq'):
    if accuracy < 1.0:
        return (df[key] >= val * (1 - accuracy)) & (df[key] <= val * (1 + accuracy))
    else:
        return get_mask_wide(df, val, accuracy=accuracy, key=key)


def get_mask_wide(df, val, accuracy=0.03, key='Freq'):
    return (df[key] >= val / accuracy) & (df[key] <= val * accuracy)


def get_upperlimit_mask(df):
    return (df['Flux'] - df['Error'] <= 0) | (df['Flux'] + df['Error'] <= 0)


def get_detection_mask(df):
    return (df['Flux'] - df['Error'] > 0) & (df['Flux'] + df['Error'] > 0)


def in_range(test_val, val, accuracy=0.03):
    return (test_val >= val * (1 - accuracy)) & (test_val <= val * (1 + accuracy))
