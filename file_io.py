import json
import os
import shutil

import numpy as np
import pandas as pn

import misc


class FileIO:
    def __init__(self, conf_filename='conf.json'):
        self.conf_filename = conf_filename
        self.conf_data = self.read_conf()

        self.obs_time, self.obs_freq, self.obs_flux, self.obs_error, self.log_obs_flux = self.read_obs_data()
        self.log_obs_time = np.log(self.obs_time)
        self.log_obs_freq = np.log(self.obs_time)
        self.log_obs_error = self.log_obs_flux - np.log(self.obs_flux - self.obs_error)

        self.grb_param_names = np.array(
            ['theta_0', 'E_K_iso', 'n_ref', 'theta_obs_frac', 'p', 'epsilon_B', 'epsilon_e_bar', 'xi_N', 'z', 'd_L'])
        self.scalefit_param_names = np.array(['theta_0', 'log10_E_K_iso_53', 'log10_n_ref', 'theta_obs_frac', 'p',
                                              'log10_epsilon_B', 'log10_epsilon_e_bar',
                                              'log10_xi_N', 'z', 'log10_d_L_28'])
        self.hyper_param_names = np.array(['log10_a1', 'log10_a2', 'log10_l1', 'log10_l2', 'log_white_noise'])
        self.fancy_scalefit_param_names = np.array(
            [r'\theta_0', r'\log_{10}(E_{K,\mathrm{iso},53})', r'\log_{10}(n_{\mathrm{ref}})',
             r'\theta_{\mathrm{obs,frac}}', r'p',
             r'\log_{10}(\epsilon_B)', r'\log_{10}(\bar{\epsilon}_e)',
             r'\log_{10}(\xi_N)', 'z', r'\log_{10}(d_{L,28})'])
        self.fancy_hyper_param_names = np.array(
            [r'\log_{10}(a_1)', r'\log_{10}(a_2)', r'\log_{10}(l_1)', r'\log_{10}(l_2)', r'\log(w)'])

        self.min_grb_params, self.max_grb_params = self.read_min_max_grb_param()

        self.scalefit_islog = self.read_scalefit_islog()
        self.hyper_islog = self.read_hyper_islog()

        self.min_hyper_params, self.max_hyper_params = self.read_min_max_hyper_param()
        self.min_scalefit_params = self.convert_grb_to_scalefit_param(self.min_grb_params)
        self.max_scalefit_params = self.convert_grb_to_scalefit_param(self.max_grb_params)

        self.free_hyper_params_mask = self.min_hyper_params != self.max_hyper_params
        self.fixed_hyper_params_mask = self.min_hyper_params == self.max_hyper_params
        self.free_scalefit_params_mask = self.min_scalefit_params != self.max_scalefit_params
        self.fixed_scalefit_params_mask = self.min_scalefit_params == self.max_scalefit_params

        self.min_free_hyper_params = self.min_hyper_params[self.free_hyper_params_mask]
        self.max_free_hyper_params = self.max_hyper_params[self.free_hyper_params_mask]
        self.fixed_hyper_params = self.min_hyper_params[self.fixed_hyper_params_mask]
        self.min_free_scalefit_params = self.min_scalefit_params[self.free_scalefit_params_mask]
        self.max_free_scalefit_params = self.max_scalefit_params[self.free_scalefit_params_mask]
        self.fixed_scalefit_params = self.min_scalefit_params[self.fixed_scalefit_params_mask]

        if self.conf_data['fit']['gp']['use_gp']:
            self.len_free_hyper_params = len(self.min_free_hyper_params)
            self.len_fixed_hyper_params = len(self.fixed_hyper_params)
        else:
            self.len_free_hyper_params = 0
            self.len_fixed_hyper_params = 0
        self.len_free_scalefit_params = len(self.min_free_scalefit_params)
        self.len_fixed_scalefit_params = len(self.fixed_scalefit_params)

        if self.conf_data['fit']['host']['fit_persistent']:
            persistent_freq_min = self.conf_data['fit']['host']['persistent_bands'][0]
            persistent_freq_max = self.conf_data['fit']['host']['persistent_bands'][1]
            self.persistent_bands = misc.group(np.unique(
                self.obs_freq[(self.obs_freq >= persistent_freq_min) & (self.obs_freq <= persistent_freq_max)]))
            print('Fit persistent flux for bands ', self.persistent_bands)
            self.min_free_persistent_params = np.zeros(len(self.persistent_bands))
            self.max_free_persistent_params = np.zeros(len(self.persistent_bands))
            self.persistent_param_names = []
            self.fancy_persistent_param_names = []
            for i, band in enumerate(self.persistent_bands):
                print(self.obs_freq[misc.in_range(self.obs_freq, band)])
                print(self.obs_flux[misc.in_range(self.obs_freq, band)])
                print(min(self.obs_flux[misc.in_range(self.obs_freq, band)]))
                print(min(self.obs_error[misc.in_range(self.obs_freq, band)]))
                print(min(self.obs_flux[misc.in_range(self.obs_freq, band)] + self.obs_error[
                    misc.in_range(self.obs_freq, band)]))
                if self.conf_data['fit']['host']['log10_flux']:
                    self.max_free_persistent_params[i] = np.log10(
                        min(self.obs_flux[misc.in_range(self.obs_freq, band)] + self.obs_error[
                            misc.in_range(self.obs_freq, band)])) + np.log10(self.conf_data['fit']['host'][
                                                                                 'max_persistent_flux_factor'])
                    # TODO make setting  for min flux
                    self.min_free_persistent_params[i] = self.max_free_persistent_params[i] - 1 - \
                                                            np.log10(self.conf_data['fit']['host'][
                                                                      'max_persistent_flux_factor'])
                else:
                    self.max_free_persistent_params[i] = min(
                        self.obs_flux[misc.in_range(self.obs_freq, band)] + self.obs_error[
                            misc.in_range(self.obs_freq, band)]) * self.conf_data['fit']['host'][
                                                             'max_persistent_flux_factor']
                    self.min_free_persistent_params[i] = 0
                self.persistent_param_names.append('f_nu_{:.2E}'.format(band))
                self.fancy_persistent_param_names.append(r'f_{\nu,%.2E}' % band)
            self.persistent_param_names = np.array(self.persistent_param_names)
            self.fancy_persistent_param_names = np.array(self.fancy_persistent_param_names)
            print(self.min_free_persistent_params, self.max_free_persistent_params)
            self.free_persistent_params_mask = self.max_free_persistent_params != self.min_free_persistent_params
            self.fixed_persistent_params_mask = self.max_free_persistent_params == self.min_free_persistent_params
            # self.fixed_persistent_params = self.min_free_persistent_params[self.fixed_persistent_params_mask]
            self.len_free_persistent_params = len(self.min_free_persistent_params)
            self.len_fixed_persistent_params = 0
            self.persistent_islog = self.read_persistent_islog()
        else:
            self.len_free_persistent_params = 0
            self.len_fixed_persistent_params = 0

        if self.conf_data['fit']['host']['fit_extinction']:
            self.len_free_extinction_params = 0
            self.len_fixed_extinction_params = 0
        else:
            self.len_free_extinction_params = 0
            self.len_fixed_extinction_params = 0
        self.len_free_params = (self.len_free_hyper_params +
                                self.len_free_scalefit_params +
                                self.len_free_persistent_params +
                                self.len_free_extinction_params)
        self.len_fixed_params = (self.len_fixed_hyper_params +
                                 self.len_fixed_scalefit_params +
                                 self.len_fixed_persistent_params +
                                 self.len_fixed_extinction_params)
        try:
            os.mkdir(self.conf_data['IO']['outdir'])
        except OSError:
            pass
        shutil.copyfile(conf_filename, self.conf_data['IO']['outdir'] + '/' + conf_filename)

    def read_conf(self):
        with open(self.conf_filename) as json_data_file:
            conf_data = json.load(json_data_file)
        return conf_data

    def read_min_max_grb_param(self):
        min_params = np.zeros(len(self.grb_param_names))
        max_params = np.zeros(len(self.grb_param_names))
        for i, name in enumerate(self.grb_param_names):
            min_params[i] = self.conf_data['fit'][name][0]
            max_params[i] = self.conf_data['fit'][name][1]
        return min_params, max_params

    def read_min_max_hyper_param(self):
        min_params = np.zeros(len(self.hyper_param_names))
        max_params = np.zeros(len(self.hyper_param_names))
        for i, name in enumerate(self.hyper_param_names):
            min_params[i] = self.conf_data['fit']['gp'][name][0]
            max_params[i] = self.conf_data['fit']['gp'][name][1]
        return min_params, max_params

    def read_hyper_islog(self):
        islog = np.full(len(self.hyper_param_names), 0)
        islog[self.hyper_param_names == 'white_noise'] = 0
        return islog

    def read_scalefit_islog(self):
        islog = np.zeros(len(self.scalefit_param_names))
        if self.conf_data['fit']['theta_0_logscale']:
            islog[self.scalefit_param_names == 'theta_0'] = 1
        return islog

    def read_persistent_islog(self):
        islog = np.zeros(len(self.persistent_param_names))
        return islog

    def convert_grb_to_scalefit_param(self, grb_param):
        scalefit_param = np.zeros(len(self.scalefit_param_names))
        scalefit_param[self.scalefit_param_names == 'theta_0'] = grb_param[self.grb_param_names == 'theta_0']
        scalefit_param[self.scalefit_param_names == 'log10_E_K_iso_53'] = np.log10(
            grb_param[self.grb_param_names == 'E_K_iso'] / 1e53)
        scalefit_param[self.scalefit_param_names == 'log10_n_ref'] = np.log10(
            grb_param[self.grb_param_names == 'n_ref'])
        scalefit_param[self.scalefit_param_names == 'theta_obs_frac'] = grb_param[
            self.grb_param_names == 'theta_obs_frac']
        scalefit_param[self.scalefit_param_names == 'p'] = grb_param[self.grb_param_names == 'p']
        scalefit_param[self.scalefit_param_names == 'log10_epsilon_B'] = np.log10(
            grb_param[self.grb_param_names == 'epsilon_B'])
        scalefit_param[self.scalefit_param_names == 'log10_epsilon_e_bar'] = np.log10(
            grb_param[self.grb_param_names == 'epsilon_e_bar'])
        scalefit_param[self.scalefit_param_names == 'log10_xi_N'] = np.log10(grb_param[self.grb_param_names == 'xi_N'])
        scalefit_param[self.scalefit_param_names == 'z'] = grb_param[self.grb_param_names == 'z']
        scalefit_param[self.scalefit_param_names == 'log10_d_L_28'] = np.log10(
            grb_param[self.grb_param_names == 'd_L'] / 1e28)
        return scalefit_param

    def convert_cube_to_scalefit_param(self, cube):
        free_scalefit_param = np.array(cube)
        mask_lin_scale = self.scalefit_islog[self.free_scalefit_params_mask] == 0
        mask_log_scale = self.scalefit_islog[self.free_scalefit_params_mask] == 1
        free_scalefit_param[mask_lin_scale] = self.min_free_scalefit_params[mask_lin_scale] + cube[mask_lin_scale] * (
                self.max_free_scalefit_params[mask_lin_scale] -
                self.min_free_scalefit_params[mask_lin_scale])
        free_scalefit_param[mask_log_scale] = 10 ** (
                np.log10(self.min_free_scalefit_params[mask_log_scale]) + cube[mask_log_scale] * (
                np.log10(self.max_free_scalefit_params[mask_log_scale]) -
                np.log10(self.min_free_scalefit_params[mask_log_scale])))
        return free_scalefit_param

    def convert_cube_to_hyper_param(self, cube):
        free_hyper_param = np.array(cube)
        mask_lin_scale = self.hyper_islog[self.free_hyper_params_mask] == 0
        mask_log_scale = self.hyper_islog[self.free_hyper_params_mask] == 1
        free_hyper_param[mask_lin_scale] = self.min_free_hyper_params[mask_lin_scale] + cube[mask_lin_scale] * (
                self.max_free_hyper_params[mask_lin_scale] -
                self.min_free_hyper_params[mask_lin_scale])
        free_hyper_param[mask_log_scale] = 10 ** (
                np.log10(self.min_free_hyper_params[mask_log_scale]) + cube[mask_log_scale] * (
                np.log10(self.max_free_hyper_params[mask_log_scale]) -
                np.log10(self.min_free_hyper_params[mask_log_scale])))
        return free_hyper_param

    def convert_cube_to_persistent_param(self, cube):
        free_persistent_param = np.array(cube)
        mask_lin_scale = self.persistent_islog[self.free_persistent_params_mask] == 0
        mask_log_scale = self.persistent_islog[self.free_persistent_params_mask] == 1
        free_persistent_param[mask_lin_scale] = self.min_free_persistent_params[mask_lin_scale] + cube[
            mask_lin_scale] * (
                                                        self.max_free_persistent_params[mask_lin_scale] -
                                                        self.min_free_persistent_params[mask_lin_scale])
        free_persistent_param[mask_log_scale] = 10 ** (
                np.log10(self.min_free_persistent_params[mask_log_scale]) + cube[mask_log_scale] * (
                np.log10(self.max_free_persistent_params[mask_log_scale]) -
                np.log10(self.min_free_persistent_params[mask_log_scale])))
        return free_persistent_param

    def read_obs_data(self):
        df = pn.read_csv(self.conf_data['fit']['obs_data'], sep=',', comment='#', header=None,
                         names=['Time', 'Freq', 'Flux', 'Error'])
        obs_time = np.array(df['Time'].values)
        obs_freq = np.array(df['Freq'].values)
        obs_flux = np.array(df['Flux'].values)
        obs_error = np.array(df['Error'].values)
        log_obs_flux = np.log(obs_flux)
        return obs_time, obs_freq, obs_flux, obs_error, log_obs_flux

    def free_param_names(self):
        free_param_names = [''] * self.len_free_params
        if self.conf_data['fit']['gp']['use_gp']:
            free_param_names[:self.len_free_hyper_params] = self.hyper_param_names[self.free_hyper_params_mask]
        free_param_names[self.len_free_hyper_params:self.len_free_hyper_params + self.len_free_scalefit_params] = \
            self.scalefit_param_names[
                self.free_scalefit_params_mask]
        if self.conf_data['fit']['host']['fit_persistent']:
            free_param_names[self.len_free_hyper_params + self.len_free_scalefit_params:
                             self.len_free_hyper_params + self.len_free_scalefit_params +
                             self.len_free_persistent_params] = self.persistent_param_names[
                self.free_persistent_params_mask]
        if self.conf_data['fit']['host']['fit_extinction']:
            pass
        return free_param_names

    def fancy_free_param_names(self):
        free_param_names = [''] * self.len_free_params
        if self.conf_data['fit']['gp']['use_gp']:
            free_param_names[:self.len_free_hyper_params] = self.fancy_hyper_param_names[self.free_hyper_params_mask]
        free_param_names[self.len_free_hyper_params:self.len_free_hyper_params + self.len_free_scalefit_params] = \
            self.fancy_scalefit_param_names[
                self.free_scalefit_params_mask]
        if self.conf_data['fit']['host']['fit_persistent']:
            free_param_names[self.len_free_hyper_params + self.len_free_scalefit_params:
                             self.len_free_hyper_params + self.len_free_scalefit_params +
                             self.len_free_persistent_params] = self.fancy_persistent_param_names[
                self.free_persistent_params_mask]
        if self.conf_data['fit']['host']['fit_extinction']:
            pass
        return free_param_names

    def order_scalefit_parameters(self, wrong_parameters):
        right_parameters = np.zeros(len(wrong_parameters))
        index_z_ = 0
        index_log10_d_L_28_ = 1
        index_log10_E_iso_53_ = 2
        index_log10_n_ref_ = 3
        index_theta_0_ = 4
        index_theta_obs_over_theta_0_ = 5
        index_p_ = 6
        index_log10_eps_e_bar_ = 7
        index_log10_eps_B_ = 8
        index_log10_xi_N_ = 9
        right_parameters[index_theta_0_] = wrong_parameters[self.scalefit_param_names == 'theta_0']
        right_parameters[index_log10_E_iso_53_] = wrong_parameters[self.scalefit_param_names == 'log10_E_K_iso_53']
        right_parameters[index_log10_n_ref_] = wrong_parameters[self.scalefit_param_names == 'log10_n_ref']
        right_parameters[index_theta_obs_over_theta_0_] = wrong_parameters[
            self.scalefit_param_names == 'theta_obs_frac']
        right_parameters[index_p_] = wrong_parameters[self.scalefit_param_names == 'p']
        right_parameters[index_log10_eps_B_] = wrong_parameters[self.scalefit_param_names == 'log10_epsilon_B']
        right_parameters[index_log10_eps_e_bar_] = wrong_parameters[self.scalefit_param_names == 'log10_epsilon_e_bar']
        right_parameters[index_log10_xi_N_] = wrong_parameters[self.scalefit_param_names == 'log10_xi_N']
        right_parameters[index_z_] = wrong_parameters[self.scalefit_param_names == 'z']
        right_parameters[index_log10_d_L_28_] = wrong_parameters[self.scalefit_param_names == 'log10_d_L_28']
        return right_parameters
