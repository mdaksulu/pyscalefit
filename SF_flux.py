"""
This module contains a class which calculates a Gamma Ray Burst afterglow
light curve directly using 2D high resolution relativistic hydro simulations.
"""
from __future__ import print_function

import os

import h5py as h5
import numpy as np


################################################################################
# Additional constants


################################################################################

class simulation_templates:
    """
    A light curve calculator for Gamma Ray Burst (GRB) afterglows.

    This class calculates the emitted synchrotron flux from a GRB afterglow
    using a pre-computed set of template tables.  Scaling relations are
    utilized in each spectral regime to obtain the flux for arbitrary values
    of the parameters.  Parameters are assumed to be given in the following
    order:
        0: 'z'  Redshift.
        1: 'log10_d_L_28' Luminosity distance (log10 of 10^28 cm).
        2: 'log10_E_iso_53' Isotropic-equivalent energy (log10 of 10^53 erg)
        3: 'log10_n_ref' Circumburst medium (proton) density, at
           reference distance 10e17 cm (log10 of cm^-3)
        4: 'theta_0' Jet half-opening angle (rad)
        5: 'theta_obs / theta_0' Off-axis observer angle (fraction of theta_0)
        6: 'p' Electron spectral index
        7: 'eps_e_bar' Energy fraction in accelerated electrons, with
           factor of (p - 2) / (p - 1) absorbed, allowing for p < 2 fits, i.e.
           eps_e_bar = eps_e * (p - 2) / (p - 1)
        8: 'eps_B' Energy fraction in magnetic field
        9: 'xi_N' Fraction of electrons accelerated.

    Public Methods:
        flux_parameters(): Calculate synchrotron parameters F_peak, nu_m,
          nu_c, and nu_a for given for given time, frequency and model parameters
        Fnu(): Calculate F_nu for given data.

    Public Variables:
        no_pars (int): (=10) number of parameters in the model.
        var_names (ndarray): Ascii names of each parameter
        var_names_fancy (ndarray): LaTeX names of parameters
    """

    # class object variables, shared among all instances of the class

    var_names = ["z", "log10_d_L_28", "log10_E_iso_53", "log10_n_ref",
                 "theta_0", "thetaobs / theta_0", "p", "log10_eps_e_bar", "log10_eps_B",
                 "xi_N"]

    no_pars = len(var_names)

    var_names_fancy = [r'$z$', r'$\log_{10} (d_{L,28})$',
                       r'$\log_{10} (E_{iso,53})$', r'$\log_{10} (n_{ref})$', r'$\theta_0$',
                       r'$\theta_{obs} / \theta_0$', r'$p$',
                       r'$\log_{10} (\bar{\epsilon}_e)$', r'$\log_{10} (\epsilon_B)$', r'$\xi_N$']

    # index entry numbers, for clarity
    index_z_ = 0
    index_log10_d_L_28_ = 1
    index_log10_E_iso_53_ = 2
    index_log10_n_ref_ = 3
    index_theta_0_ = 4
    index_theta_obs_over_theta_0_ = 5
    index_p_ = 6
    index_log10_eps_e_bar_ = 7
    index_log10_eps_B_ = 8
    index_log10_xi_N_ = 9

    # same as previous, using condensed notation that leaves log / linear and
    # theta_obs units implicit
    index_d_L_28_ = 1
    index_E_iso_53_ = 2
    index_n_ref_ = 3
    index_theta_obs_ = 5
    index_eps_e_bar_ = 7
    index_eps_B_ = 8
    index_xi_N_ = 9

    # -----------------------------------------------------------------------------
    # Function definitions

    # Constructor function
    def __init__(self, table, extrapolate=(-np.inf, np.inf)):
        """
        Initialize a ScaleGen instance.

        Read the HDF5 model data into memory, setup interpolator, and set
        which parameters will be log-scaled.

        Args:
            table (string): path and filename for HDF5 table containing the
                            model data.

        Kwargs:
            extrapolate (tuple): Allow extrapolation in tau to a minimum of
                                extrapolate[0] and a maximum of extrapolate[1].
                                'theta_0' and 'theta_obs' are never
                                extrapolated.

        Returns:
             ScaleGen instance.
        """

        # instance object variables, generated separately for each class instance

        self._cube_filename = None

        # datacube header info
        self.k = None  # circumburst density slope k, either 0 or 2
        self.Ebase = None
        self.nbase = None
        self.dLbase = None
        self.ksi_N_base = None

        # datacube data
        self._fp_cube = None
        self._fm_cube = None
        self._fc_cube = None
        self._fa_cube = None
        self._tau_axis = None
        self._th0_axis = None
        self._thobs_axis = None

        # log10 version of datacube data
        self._log_fp_cube = None
        self._log_fm_cube = None
        self._log_fc_cube = None
        self._log_fa_cube = None
        self._log_tau_axis = None
        self._log_th0_axis = None

        self._lin_params = None
        self._log10_params = None

        self._interpolate_cubes = None

        SrcDir = os.path.dirname(os.path.abspath(__file__))

        self._cube_filename = table
        self._load_cubes()
        self._extrapolate = extrapolate

        # switches whether to include synchrotron self-absorption and
        # electron cooling.
        self.absorption = True
        self.cooling = True
        self.smooth = True  # True # Smooth spectra

    # -----------------------------------------------------------------------------

    def _load_cubes(self):
        # Load the HDF5 model data into memory.

        # open hdf5 file for reading
        cubes = h5.File(self._cube_filename, 'r')

        # load header information
        self.Ebase = (cubes['E_iso'])[0] / 1e53
        self.nbase = (cubes['n_ref'])[0]
        self.dLbase = (cubes['d_L'])[0] / 1e28
        self.k = (cubes['k'])[0]
        # self.Ebase = 1
        # self.nbase = 29.98
        # self.dLbase = 1
        # self.k = 0
        # This allows for a luminosity distance different from 1e28 being used
        # in compiling the table, and for E_iso and n_ref different from
        # 1e53 erg and 1 cm^-3 respectively. In practice, all tables will be
        # compiled using these baseline values (i.e. NOT 29.89 for n_ref in the
        # wind case; here the 'typical' value differs from the 'baseline' value).
        # The functions fp, fm, fc, fa are always with their dependencies on
        # redshift z and microphysics parameters epsilon_B, epsilon_e, xi_N
        # removed, which, in terms of `baselines' amounts to z = 0, epsilon_B =
        # epsilon_e = xi_N = 1. Also, p is assumed to be set at p = 2.5

        # load data
        # fp = np.array(cubes['F_peak'])
        self._th0_axis = np.array(cubes['theta_0'])
        self._thobs_axis = np.array(cubes['theta_obs'])
        self._tau_axis = np.array(cubes['t'])
        self._fp_cube = np.array(cubes['F_peak'])
        self._fm_cube = np.array(cubes['nu_m'])
        self._fc_cube = np.array(cubes['nu_c'])
        self._fa_cube = np.array(cubes['nu_a'])

        Zeros = np.where(self._fp_cube == 0)
        self._fp_cube[Zeros] = 1.0e-300

        Zeros = np.where(self._fm_cube == 0)
        self._fm_cube[Zeros] = 1.0e-300

        Zeros = np.where(self._fc_cube == 0)
        self._fc_cube[Zeros] = 1.0e-300

        Zeros = np.where(self._fa_cube == 0)
        self._fa_cube[
            Zeros] = 1.0e-300  #######################################################!!!!!!!!!!!!!!!!!!!!!!!!!

        # close hdf5 file
        cubes.close()

        # generate logarithms
        self._log_fp_cube = np.log10(self._fp_cube)
        self._log_fm_cube = np.log10(self._fm_cube)
        self._log_fc_cube = np.log10(self._fc_cube)
        self._log_fa_cube = np.log10(self._fa_cube)
        self._log_tau_axis = np.log10(self._tau_axis)
        self._log_th0_axis = np.log10(self._th0_axis)

    # -----------------------------------------------------------------------------

    def _get_axis_coord(self, axis, value):
        # Return location of 'value' along 'axis', interpolating linearly.

        upper_coord = np.searchsorted(axis, value)
        upper_coord = np.clip(upper_coord, 1, len(axis) - 1)

        coord = (upper_coord -
                 (axis[upper_coord] - value) / (axis[upper_coord] - axis[upper_coord - 1]))

        # upper_coord is an integer corresponding to the entry index for the first
        # entry with a value higher than function argument 'value' (i.e. the first
        # entry to the right).
        # coord is a double between upper_coord and upper_coord - 1 and reflects
        # where between the two bounding entry values the provided 'value' sits.
        return coord, upper_coord

    # -----------------------------------------------------------------------------

    def _interpolate_trilin(self, taus, th0, thobs):
        # Perform trilinear interpolation at (taus,th0,thobs) on given
        # model-cubes with axes. Interpolate in log space for characteristic
        # frequencies, theta_0 and tau, but linear space for theta_obs/theta_0

        tau_coords, upper_tau_coords = self._get_axis_coord(self._log_tau_axis,
                                                            np.log10(taus))
        th0_coord, upper_th0_coord = self._get_axis_coord(self._log_th0_axis,
                                                          np.log10(th0))
        thobs_coord, upper_thobs_coord = self._get_axis_coord(self._thobs_axis,
                                                              thobs)

        # Set interpolation weights
        w_tau_u = tau_coords - upper_tau_coords + 1
        w_tau_l = upper_tau_coords - tau_coords
        w_th0_u = th0_coord - upper_th0_coord + 1
        w_th0_l = upper_th0_coord - th0_coord
        w_thobs_u = thobs_coord - upper_thobs_coord + 1
        w_thobs_l = upper_thobs_coord - thobs_coord

        min_t = upper_tau_coords.min() - 1
        max_t = upper_tau_coords.max() + 1
        shifted_tau_coords = upper_tau_coords - min_t
        fp_tube = np.zeros(max_t - min_t)
        fp_tube += w_th0_l * w_thobs_l * self._log_fp_cube[min_t:max_t,
                                         upper_th0_coord - 1, upper_thobs_coord - 1]
        fp_tube += w_th0_l * w_thobs_u * self._log_fp_cube[min_t:max_t,
                                         upper_th0_coord - 1, upper_thobs_coord]
        fp_tube += w_th0_u * w_thobs_l * self._log_fp_cube[min_t:max_t,
                                         upper_th0_coord, upper_thobs_coord - 1]
        fp_tube += w_th0_u * w_thobs_u * self._log_fp_cube[min_t:max_t,
                                         upper_th0_coord, upper_thobs_coord]

        fm_tube = np.zeros(max_t - min_t)
        fm_tube = w_th0_l * w_thobs_l * self._log_fm_cube[min_t:max_t,
                                        upper_th0_coord - 1, upper_thobs_coord - 1]
        fm_tube += w_th0_l * w_thobs_u * self._log_fm_cube[min_t:max_t,
                                         upper_th0_coord - 1, upper_thobs_coord]
        fm_tube += w_th0_u * w_thobs_l * self._log_fm_cube[min_t:max_t,
                                         upper_th0_coord, upper_thobs_coord - 1]
        fm_tube += w_th0_u * w_thobs_u * self._log_fm_cube[min_t:max_t,
                                         upper_th0_coord, upper_thobs_coord]

        fc_tube = np.zeros(max_t - min_t)
        fc_tube = w_th0_l * w_thobs_l * self._log_fc_cube[min_t:max_t,
                                        upper_th0_coord - 1, upper_thobs_coord - 1]
        fc_tube += w_th0_l * w_thobs_u * self._log_fc_cube[min_t:max_t,
                                         upper_th0_coord - 1, upper_thobs_coord]
        fc_tube += w_th0_u * w_thobs_l * self._log_fc_cube[min_t:max_t,
                                         upper_th0_coord, upper_thobs_coord - 1]
        fc_tube += w_th0_u * w_thobs_u * self._log_fc_cube[min_t:max_t,
                                         upper_th0_coord, upper_thobs_coord]

        fa_tube = np.zeros(max_t - min_t)
        fa_tube = w_th0_l * w_thobs_l * self._log_fa_cube[min_t:max_t,
                                        upper_th0_coord - 1, upper_thobs_coord - 1]
        fa_tube += w_th0_l * w_thobs_u * self._log_fa_cube[min_t:max_t,
                                         upper_th0_coord - 1, upper_thobs_coord]
        fa_tube += w_th0_u * w_thobs_l * self._log_fa_cube[min_t:max_t,
                                         upper_th0_coord, upper_thobs_coord - 1]
        fa_tube += w_th0_u * w_thobs_u * self._log_fa_cube[min_t:max_t,
                                         upper_th0_coord, upper_thobs_coord]

        fps = (w_tau_l * fp_tube[shifted_tau_coords - 1] +
               w_tau_u * fp_tube[shifted_tau_coords])
        fms = (w_tau_l * fm_tube[shifted_tau_coords - 1] +
               w_tau_u * fm_tube[shifted_tau_coords])
        fcs = (w_tau_l * fc_tube[shifted_tau_coords - 1] +
               w_tau_u * fc_tube[shifted_tau_coords])
        fas = (w_tau_l * fa_tube[shifted_tau_coords - 1] +
               w_tau_u * fa_tube[shifted_tau_coords])

        return 10 ** (fps), 10 ** (fms), 10 ** (fcs), 10 ** (fas)
        # return np.full(len(taus), 9.4e-3), np.full(len(taus), 4.5e29), np.full(len(taus), 1.71e11), np.full(len(taus),
        #                                                                                                     243.06)

    # -----------------------------------------------------------------------------

    def flux_parameters(self, times, params):
        """
        Calculate the synctrotron spectrum peak flux and break frequecies.

        For parameters 'params,' calculate the F_peak, nu_m, nu_a and nu_c at
        'times.'

        Args:
            times (ndarray): Array (1 dimensional) of observer times at which
              to calculate spectrum.
            params (ndarray): Values of model parameters for which to calculate
              spectrum.  Must be of length self.ndim

        Returns:
            (F_peak, nu_m, nu_c, nu_a1)
            F_peak (ndarray): The peak of the synchrotron spectrum in mJy.
              Same length as 'times'.
            nu_m (ndarray): Break frequency 'nu_m' in Hz. Same length as 'times'.
            nu_c (ndarray): Break frequency 'nu_c' in Hz. Same length as 'times'.
            nu_a1 (ndarray): Break frequency 'nu_a1' in Hz. Same length as 'times'.
              Note that nu_a will be a different value for spectra other than 1
        """

        # Ensure we have linear scale versions of all parameters as well
        parslin = np.array(params)  # creates instance of params, not just a pointer
        parslin[self.index_d_L_28_] = 10. ** params[self.index_d_L_28_]
        parslin[self.index_E_iso_53_] = 10. ** params[self.index_E_iso_53_]
        parslin[self.index_n_ref_] = 10. ** params[self.index_n_ref_]
        parslin[self.index_eps_e_bar_] = 10. ** params[self.index_eps_e_bar_]
        parslin[self.index_eps_B_] = 10. ** params[self.index_eps_B_]
        parslin[self.index_xi_N_] = 10. ** params[self.index_xi_N_]

        # scale time from cgs to 'tau'
        taus = (times *
                (parslin[self.index_E_iso_53_] * self.nbase /
                 (self.Ebase * parslin[self.index_n_ref_])) **
                (-1. / (3. - self.k)) /
                (1. + parslin[self.index_z_]))
        # taus = times

        # obtain baseline values for characteristic quantities, which do not
        # yet account for emission parameters, redshift, actual energy, density
        # and luminosity distance scale, and p-dependent pre-factor
        fp, fm, fc, fa = self._interpolate_trilin(taus,
                                                  parslin[self.index_theta_0_], parslin[self.index_theta_obs_])
        # print("f_p = %1.2e, fa = %1.2e, fm = %1.2e, fc = %1.2e" % (fp[0], fa[0], fm[0], fc[0]))

        # fp[taus < self._extrapolate[0]] = np.inf
        # fp[taus > self._extrapolate[1]] = np.inf

        # Disable extrapolation by immediately pushing chi^2 to infinity
        # fp[taus < self._tau_axis[0]] = 0.
        # fp[taus > self._tau_axis[-1]] = 0.

        # include redshift, scale to appropriate energy and density etc.
        peak_flux = (fp *
                     (1. + parslin[self.index_z_]) * parslin[self.index_d_L_28_] ** (-2) *
                     (parslin[self.index_p_] - 1.) / (3. * parslin[self.index_p_] - 1.) *
                     (parslin[self.index_E_iso_53_] / self.Ebase) ** (3. * (2. - self.k) /
                                                                      (2. * (3. - self.k))) *
                     (parslin[self.index_n_ref_] / self.nbase) ** (3. / (2. * (3. - self.k))) *
                     parslin[self.index_eps_B_] ** 0.5 *
                     parslin[self.index_xi_N_])

        nu_m = (fm /
                (1. + parslin[self.index_z_]) *
                parslin[self.index_eps_e_bar_] ** 2 *
                parslin[self.index_eps_B_] ** 0.5 /
                parslin[self.index_xi_N_] ** 2 *
                (parslin[self.index_E_iso_53_] / self.Ebase) ** (-self.k /
                                                                 (2. * (3. - self.k))) *
                (parslin[self.index_n_ref_] / self.nbase) ** (3. / (2. * (3. - self.k))))

        nu_c = (fc /
                (1. + parslin[self.index_z_]) *
                parslin[self.index_eps_B_] ** (-3. / 2.) *
                (parslin[self.index_E_iso_53_] / self.Ebase) ** ((3. * self.k - 4.) /
                                                                 (2. * (3. - self.k))) *
                (parslin[self.index_n_ref_] / self.nbase) ** (-5. / (2. * (3. - self.k))))

        nu_a = (fa /
                (1. + parslin[self.index_z_]) *
                (parslin[self.index_p_] - 1.) ** (3. / 5.) /
                (parslin[self.index_p_] + 2.) ** (3. / 5.) /
                parslin[self.index_eps_e_bar_] *
                parslin[self.index_eps_B_] ** (1. / 5.) *
                parslin[self.index_xi_N_] ** (8. / 5.) *
                (parslin[self.index_E_iso_53_] / self.Ebase) ** ((3. - 4. * self.k) /
                                                                 (5. * (3. - self.k))) *
                (parslin[self.index_n_ref_] / self.nbase) ** (9. / (5. * (3. - self.k))))

        # TO DO DOUBLECHECK (p - 1), (p - 2) etc factors!!!!!

        # return peak_flux, nu_m, nu_c, nu_a
        return peak_flux, nu_m, nu_c, nu_a

    # -----------------------------------------------------------------------------

    def Fnu(self, params, times, nu):
        """
        Calculate the spectral flux (F_nu) light curve.

        For given model parameters calculate F_nu at each time and frequency.

        Args:
          times (ndarray): Observer times (in seconds) at which to calculate
            flux.  Must be 1d.
          nu (ndarray): Frequencies at which to calculate each flux,
            in Hz. Same shape as 'times'
          params (ndarray): Model parameters for which to calculate light
            curve.  Must be 1d, of length 'self.ndim'

        Returns:
            ndarray: The spectral flux F_nu at each (time, freq) value in mJy.
                    Same shape as 'times'.
        """

        p = params[self.index_p_]
        peak_flux, nu_m, nu_c, nu_a = self.flux_parameters(times, params)
        # print("f_p = %1.2e, nu_a = %1.2e, nu_m = %1.2e, nu_c = %1.2e" % (peak_flux[0], nu_a[0], nu_m[0], nu_c[0]))

        output = np.empty(len(times))

        # We can compute the self-absorption breaks for spectral orderings other
        # than nu_a1 < nu_m1 < nu_c1 in terms of these breaks.

        if self.absorption == True and self.cooling == True:  # standard
            # spectrum1 = (nu_a <= nu_m) * (nu_m <= nu_c)
            # spectrum2 = (nu_m <= nu_a) * (nu_a <= nu_c)
            # spectrum3 = (nu_m <= nu_c) * (nu_c <= nu_a)
            # spectrum3alt = (nu_c <= nu_m) * (nu_m <= nu_a)
            # spectrum4 = (nu_c <= nu_a) * (nu_a <= nu_m)
            # spectrum5 = ((~spectrum1) * (~spectrum2) * (~spectrum3) * (~spectrum3alt) *
            #              (~spectrum4))
            # # print(spectrum1[0], spectrum2[0], spectrum3[0], spectrum3alt[0], spectrum4[0], spectrum5[0])
            # for i in range(len(nu_a)):
            #     if spectrum2[i] == 1:
            #         nu_a[i] = (nu_a[i] / (nu_m[i] ** ((2. + 3. * p) / (3. * (p + 4.))))) ** ((3. * (p + 4.)) / 10.)
            #     if spectrum3[i] == 1:
            #         nu_a[i] = (nu_a[i] / (nu_m[i] ** ((2. + 3. * p) / (3. * (p + 5.))) *
            #                               nu_c[i] ** (1. / (p + 5.)))) ** ((3. * (p + 5.)) / 10)
            #     if spectrum3alt[i] == 1:
            #         nu_a[i] = (nu_a[i] / (nu_m[i] ** ((3. * p + 5.) / (3. * (p + 5.))))) ** ((3. * (p + 5.)) / 10.)
            #     if spectrum4[i] == 1:
            #         nu_a[i] = (nu_a[i] / (nu_m[i] ** (1. / 3.))) ** (3. / 2.)
            #     if spectrum5[i] == 1:
            #         nu_a[i] = (nu_a[i] / (nu_m[i] ** 0.5 * nu_c[i] ** (-0.5)))
            #
            #     print(spectrum1[i], spectrum2[i], spectrum3[i], spectrum3alt[i], spectrum4[i], spectrum5[i])
            # nu_a = (nu_a / (nu_m ** 0.5 * nu_c ** (-0.5)))
            nu_a2 = (nu_a ** (10. / (3. * (p + 4.))) *
                     nu_m ** ((2. + 3. * p) / (3. * (p + 4.))))
            nu_a3 = (nu_a ** (10. / (3 * (p + 5.))) *
                     nu_m ** ((2. + 3. * p) / (3. * (p + 5.))) *
                     nu_c ** (1. / (p + 5.)))
            nu_a3alt = (nu_a ** (10. / (3. * (p + 5.))) *
                        nu_m ** ((3. * p + 5.) / (3. * (p + 5.))))
            nu_a4 = (nu_a ** (2. / 3.) * nu_m ** (1. / 3.))
            nu_a5 = (nu_a * nu_m ** 0.5 * nu_c ** (-0.5))

            # first division into subsets, by spectrum

            spectrum1 = (nu_a <= nu_m) * (nu_m <= nu_c)
            spectrum2 = (nu_m <= nu_a2) * (nu_a2 <= nu_c)
            spectrum3 = (nu_m <= nu_c) * (nu_c <= nu_a3)
            spectrum3alt = (nu_c <= nu_m) * (nu_m <= nu_a3alt)
            spectrum4 = (nu_c <= nu_a4) * (nu_a4 <= nu_m)
            spectrum5 = ((~spectrum1) * (~spectrum2) * (~spectrum3) * (~spectrum3alt) *
                         (~spectrum4))

            # Smooth spectra
            if self.smooth == True:
                w1 = (nu_c / nu_a) ** 2.5 * 1
                w2 = (nu_c / nu_m) ** 2.5 * 1
                w3 = (nu_a3 / nu_m) ** 2.5 * 1
                w3alt = (nu_a3alt / nu_c) ** 2.5 * 1
                w4 = (nu_m / nu_c) ** 2.5 * 1
                w5 = (nu_m / nu_a5) ** 2.5 * 1

                # print("w = %1.2e, %1.2e, %1.2e, %1.2e, %1.2e, %1.2e" % (w1[0], w2[0], w3[0], w3alt[0], w4[0], w5[0]))
                # print ("nu_a = %1.2e, %1.2e, %1.2e, %1.2e, %1.2e" % (nu_a[0], nu_a2[0], nu_a3[0], nu_a4[0], nu_a5[0]))
                # print ("nu_ap = %1.2e" % nu_ap[0])
                # print ("nu_m = %1.2e, nu_c = %1.2e" % (nu_m[0], nu_c[0]))

                # spectrum 1
                s1 = 1.64
                s2 = 1.84 - 0.4 * p
                s3 = 1.15 - 0.06 * p
                b1 = 2.
                b2 = 1. / 3.
                b3 = 0.5 * (1. - p)
                b4 = -0.5 * p
                c = peak_flux * (nu_a / nu_m) ** (b2)

                output1 = (c * ((nu / nu_a) ** (-(s1 * b1)) +
                                (nu / nu_a) ** (-(s1 * b2))) ** (-1. / s1) *
                           (1. + (nu / nu_m) ** (s2 * (b2 - b3))) ** (-1. / s2) *
                           (1. + (nu / nu_c) ** (s3 * (b3 - b4))) ** (-1. / s3))

                # spectrum 2
                s1 = -(3.44 * p - 1.141)
                s2 = 1.47 - 0.21 * p
                s3 = 1.15 - 0.06 * p
                b1 = 2.
                b2 = 5. / 2.
                b3 = 0.5 * (1. - p)
                b4 = -0.5 * p
                c = peak_flux * (nu_a2 / nu_m) ** (b3 - b2)

                output2 = (c * ((nu / nu_m) ** (-(s1 * b1)) +
                                (nu / nu_m) ** (-(s1 * b2))) ** (-1. / s1) *
                           (1. + (nu / nu_a2) ** (s2 * (b2 - b3))) ** (-1. / s2) *
                           (1. + (nu / nu_c) ** (s3 * (b3 - b4))) ** (-1. / s3))

                # spectrum 3
                s1 = -(3.44 * p - 1.41)
                s2 = 0.94 - 0.14 * p
                b1 = 2.
                b2 = 5. / 2.
                b3 = -0.5 * p
                c = peak_flux * (nu_a3 / nu_m) ** (-0.5 * (1. - p) - 5. / 2.)

                output3 = (c * ((nu / nu_m) ** (-(s1 * b1)) +
                                (nu / nu_m) ** (-(s1 * b2))) ** (-1. / s1) *
                           (1. + (nu / nu_a3) ** (s2 * (b2 - b3))) ** (-1. / s2))

                # spectrum 3alt
                s1 = -(3.44 * p - 1.41)
                s2 = 0.94 - 0.14 * p
                b1 = 2.
                b2 = 5. / 2.
                b3 = -0.5 * p
                c = (peak_flux * (nu_a3alt / nu_m) ** (1. / 3.) * (nu_m / nu_a3alt) ** 2. *
                     (nu_m / nu_c) ** (-0.5))

                output3alt = (c * ((nu / nu_m) ** (-(s1 * b1)) +
                                   (nu / nu_m) ** (-(s1 * b2))) ** (-1. / s1) *
                              (1. + (nu / nu_a3alt) ** (s2 * (b2 - b3))) ** (-1. / s2))

                # spectrum 4
                s1 = 0.907
                s2 = 3.34 - 0.82 * p
                b1 = 2.
                b2 = 1. / 3.
                b3 = 0.5 * (1. - p)
                c = peak_flux * (nu_a4 / nu_m) ** (-0.5)

                output4 = (c * ((nu / nu_a4) ** (-(s1 * b1)) +
                                (nu / nu_a4) ** (-(s1 * b2))) ** (-1. / s1) *
                           (1. + (nu / nu_m) ** (s2 * (b2 - b3))) ** (-1. / s2))

                # spectrum 5
                s1 = 1.99 - 0.04 * p
                s2 = 0.597
                s3 = 3.34 - 0.82 * p
                b1 = 2.
                b2 = 1. / 3.
                b3 = -0.5
                b4 = -0.5 * p
                c = peak_flux * (nu_a5 / nu_c) ** (1. / 3.)

                output5 = (c * ((nu / nu_a5) ** (-(s1 * b1)) +
                                (nu / nu_a5) ** (-(s1 * b2))) ** (-1. / s1) *
                           (1. + (nu / nu_c) ** (s2 * (b2 - b3))) ** (-1. / s2) *
                           (1. + (nu / nu_m) ** (s3 * (b3 - b4))) ** (-1. / s3))

                # combine smooth spectra

                output = ((w1 * output1 + w2 * output2 + w3 * output3 +
                           w3alt * output3alt + w4 * output4 + w5 * output5) /
                          (w1 + w2 + w3 + w3alt + w4 + w5))
                return output

            # Sharp spectra  ######################################################################### use nu_a2, nu_a3 etc as well here !!!!!!!!!!

            inu_a = 1.0 / nu_a
            inu_m = 1.0 / nu_m
            inu_c = 1.0 / nu_c
            bp = 0.5 * (1. - p)
            bpc = -0.5 * p

            # further divide the list of requested measurements into subsets covering
            # the appropriate spectral regimes

            spectrum1B = spectrum1 * (nu <= nu_a)
            spectrum1D = spectrum1 * (nu > nu_a) * (nu <= nu_m)
            spectrum1G = spectrum1 * (nu > nu_m) * (nu < nu_c)
            spectrum1H = spectrum1 * (nu > nu_c)

            spectrum2B = spectrum2 * (nu <= nu_m)
            spectrum2A = spectrum2 * (nu > nu_m) * (nu <= nu_a2)
            spectrum2G = spectrum2 * (nu > nu_a2) * (nu <= nu_c)
            spectrum2H = spectrum2 * (nu > nu_c)

            spectrum3B = spectrum3 * (nu <= nu_m)
            spectrum3A = spectrum3 * (nu > nu_m) * (nu <= nu_a3)
            spectrum3H = spectrum3 * (nu > nu_a3)

            spectrum3altK = spectrum3alt * (nu <= nu_m)
            spectrum3altL = (spectrum3alt * (nu > nu_m) *
                             (nu <= nu_a3alt))
            spectrum3altH = spectrum3alt * (nu > nu_a3alt)

            spectrum4K = spectrum4 * (nu <= nu_a4)
            spectrum4F = spectrum4 * (nu > nu_a4) * (nu <= nu_m)
            spectrum4H = spectrum4 * (nu > nu_m)

            spectrum5K = spectrum5 * (nu <= nu_a5)
            spectrum5E = spectrum5 * (nu > nu_a5) * (nu <= nu_c)
            spectrum5F = spectrum5 * (nu > nu_c) * (nu <= nu_m)
            spectrum5H = spectrum5 * (nu > nu_m)

            # inu_a = np.zeros(len(nu_a))
            # for i in range(len(nu_a)):
            #     if spectrum2[i] == 1:
            #         inu_a[i] = 1.0 / nu_a2[i]
            #         nu_a[i] = nu_a2[i]
            #     elif spectrum3[i] == 1:
            #         inu_a[i] = 1.0 / nu_a3[i]
            #         nu_a[i] = nu_a3[i]
            #     elif spectrum3alt[i] == 1:
            #         inu_a[i] = 1.0 / nu_a3alt[i]
            #         nu_a[i] = nu_a3alt[i]
            #     elif spectrum4[i] == 1:
            #         inu_a[i] = 1.0 / nu_a4[i]
            #         nu_a[i] = nu_a4[i]
            #     elif spectrum5[i] == 1:
            #         inu_a[i] = 1.0 / nu_a5[i]
            #         nu_a[i] = nu_a5[i]
            #     else:
            #         inu_a[i] = 1.0 / nu_a[i]
            # spectrum 1 output
            output[spectrum1B] = (peak_flux[spectrum1B] *
                                  (nu_a[spectrum1B] * inu_m[spectrum1B]) ** (1. / 3.) *
                                  (nu[spectrum1B] * inu_a[spectrum1B]) ** (2.))

            output[spectrum1D] = (peak_flux[spectrum1D] *
                                  (nu[spectrum1D] * inu_m[spectrum1D]) ** (1. / 3.))

            output[spectrum1G] = (peak_flux[spectrum1G] *
                                  (nu[spectrum1G] * inu_m[spectrum1G]) ** bp)

            output[spectrum1H] = (peak_flux[spectrum1H] *
                                  (nu_c[spectrum1H] * inu_m[spectrum1H]) ** (bp) *
                                  (nu[spectrum1H] * inu_c[spectrum1H]) ** bpc)

            # spectrum 2 output
            output[spectrum2B] = (peak_flux[spectrum2B] *
                                  (nu_a[spectrum2B] * inu_m[spectrum2B]) ** (1. / 3.) *
                                  (nu[spectrum2B] * inu_a[spectrum2B]) ** (2.))

            output[spectrum2A] = (peak_flux[spectrum2A] *
                                  (nu_a[spectrum2A] * inu_m[spectrum2A]) ** (-5. / 3.) *
                                  (nu[spectrum2A] * inu_m[spectrum2A]) ** (5. / 2.))

            output[spectrum2G] = (peak_flux[spectrum2G] *
                                  (nu[spectrum2G] * inu_m[spectrum2G]) ** bp)

            output[spectrum2H] = (peak_flux[spectrum2H] *
                                  (nu_c[spectrum2H] * inu_m[spectrum2H]) ** (bp) *
                                  (nu[spectrum2H] * inu_c[spectrum2H]) ** bpc)

            # spectrum 3 output
            output[spectrum3B] = (peak_flux[spectrum3B] *
                                  (nu_a[spectrum3B] * inu_m[spectrum3B]) ** (1. / 3.) *
                                  (nu[spectrum3B] * inu_a[spectrum3B]) ** (2.))

            output[spectrum3A] = (peak_flux[spectrum3A] *
                                  (nu_a[spectrum3A] * inu_m[spectrum3A]) ** (-5. / 3.) *
                                  (nu[spectrum3A] * inu_m[spectrum3A]) ** (5. / 2.))

            output[spectrum3H] = (peak_flux[spectrum3H] *
                                  (nu_c[spectrum3H] * inu_m[spectrum3H]) ** (bp) *
                                  (nu[spectrum3H] * inu_c[spectrum3H]) ** bpc)

            # spectrum 3alt output
            output[spectrum3altK] = (peak_flux[spectrum3altK] *
                                     (nu_a[spectrum3altK] * inu_m[spectrum3altK]) ** (1. / 3.) *
                                     (nu[spectrum3altK] * inu_a[spectrum3altK]) ** (2.) *
                                     (nu_m[spectrum3altK] * inu_c[spectrum3altK]) ** (-0.5))

            output[spectrum3altL] = (peak_flux[spectrum3altL] *
                                     (nu_a[spectrum3altL] * inu_m[spectrum3altL]) ** (-5. / 3.) *
                                     (nu[spectrum3altL] * inu_m[spectrum3altL]) ** (5. / 2.) *
                                     (nu_m[spectrum3altL] * inu_c[spectrum3altL]) ** (-0.5))

            output[spectrum3altH] = (peak_flux[spectrum3altH] *
                                     (nu_c[spectrum3altH] * inu_m[spectrum3altH]) ** (bp) *
                                     (nu[spectrum3altH] * inu_c[spectrum3altH]) ** bpc)

            # spectrum 4 output
            output[spectrum4K] = (peak_flux[spectrum4K] *
                                  (nu_a[spectrum4K] * inu_m[spectrum4K]) ** (1. / 3.) *
                                  (nu[spectrum4K] * inu_a[spectrum4K]) ** (2.) *
                                  (nu_m[spectrum4K] * inu_c[spectrum4K]) ** (-0.5))

            output[spectrum4F] = (peak_flux[spectrum4F] *
                                  (nu[spectrum4F] * inu_c[spectrum4F]) ** (-0.5))

            output[spectrum4H] = (peak_flux[spectrum4H] *
                                  (nu_c[spectrum4H] * inu_m[spectrum4H]) ** (bp) *
                                  (nu[spectrum4H] * inu_c[spectrum4H]) ** bpc)

            # spectrum 5 output
            output[spectrum5K] = (peak_flux[spectrum5K] *
                                  (nu_a[spectrum5K] * inu_m[spectrum5K]) ** (1. / 3.) *
                                  (nu[spectrum5K] * inu_a[spectrum5K]) ** (2.)
                                  * (nu_m[spectrum5K] * inu_c[spectrum5K]) ** (-0.5))

            output[spectrum5E] = (peak_flux[spectrum5E] *
                                  (nu[spectrum5E] * inu_c[spectrum5E]) ** (1. / 3.))

            output[spectrum5F] = (peak_flux[spectrum5F] *
                                  (nu[spectrum5F] * inu_c[spectrum5F]) ** (-0.5))

            output[spectrum5H] = (peak_flux[spectrum5H] *
                                  (nu_c[spectrum5H] * inu_m[spectrum5H]) ** (bp) *
                                  (nu[spectrum5H] * inu_c[spectrum5H]) ** bpc)

        elif self.absorption == False and self.cooling == False:

            inu_m = 1.0 / nu_m
            bp = 0.5 * (1. - p)
            bpc = -0.5 * p

            spectrum1D = (nu <= nu_m)
            spectrum1G = (nu > nu_m)

            output[spectrum1D] = (peak_flux[spectrum1D] *
                                  (nu[spectrum1D] * inu_m[spectrum1D]) ** (1. / 3.))

            output[spectrum1G] = (peak_flux[spectrum1G] *
                                  (nu[spectrum1G] * inu_m[spectrum1G]) ** bp)

        elif self.absorption == False and self.cooling == True:

            inu_m = 1.0 / nu_m
            inu_c = 1.0 / nu_c
            bp = 0.5 * (1. - p)
            bpc = -0.5 * p

            spectrum1 = (nu_m <= nu_c)
            spectrum5 = (~spectrum1)

            spectrum1D = spectrum1 * (nu <= nu_m)
            spectrum1G = spectrum1 * (nu > nu_m) * (nu < nu_c)
            spectrum1H = spectrum1 * (nu > nu_c)

            spectrum5E = spectrum5 * (nu <= nu_c)
            spectrum5F = spectrum5 * (nu > nu_c) * (nu <= nu_m)
            spectrum5H = spectrum5 * (nu > nu_m)

            # spectrum 1 output
            output[spectrum1D] = (peak_flux[spectrum1D] *
                                  (nu[spectrum1D] * inu_m[spectrum1D]) ** (1. / 3.))

            output[spectrum1G] = (peak_flux[spectrum1G] *
                                  (nu[spectrum1G] * inu_m[spectrum1G]) ** bp)

            output[spectrum1H] = (peak_flux[spectrum1H] *
                                  (nu_c[spectrum1H] * inu_m[spectrum1H]) ** (bp) *
                                  (nu[spectrum1H] * inu_c[spectrum1H]) ** bpc)

            # spectrum 5 output
            output[spectrum5E] = (peak_flux[spectrum5E] *
                                  (nu[spectrum5E] * inu_c[spectrum5E]) ** (1. / 3.))

            output[spectrum5F] = (peak_flux[spectrum5F] *
                                  (nu[spectrum5F] * inu_c[spectrum5F]) ** (-0.5))

            output[spectrum5H] = (peak_flux[spectrum5H] *
                                  (nu_c[spectrum5H] * inu_m[spectrum5H]) ** (bp) *
                                  (nu[spectrum5H] * inu_c[spectrum5H]) ** bpc)

        elif self.absorption == True and self.cooling == False:

            nu_a2 = (nu_a ** (10. / (3. * (p + 4.))) *
                     nu_m ** ((2. + 3. * p) / (3. * (p + 4.))))

            inu_m = 1.0 / nu_m
            inu_a = 1.0 / nu_a
            bp = 0.5 * (1. - p)
            bpc = -0.5 * p

            spectrum1 = (nu_a <= nu_m)
            spectrum2 = (~spectrum1)

            spectrum1B = spectrum1 * (nu <= nu_a)
            spectrum1D = spectrum1 * (nu > nu_a) * (nu <= nu_m)
            spectrum1G = spectrum1 * (nu > nu_m)

            spectrum2B = spectrum2 * (nu <= nu_m)
            spectrum2A = spectrum2 * (nu > nu_m) * (nu <= nu_a2)
            spectrum2G = spectrum2 * (nu > nu_a2)

            # spectrum 1 output
            output[spectrum1B] = (peak_flux[spectrum1B] *
                                  (nu_a[spectrum1B] * inu_m[spectrum1B]) ** (1. / 3.) *
                                  (nu[spectrum1B] * inu_a[spectrum1B]) ** (2.))

            output[spectrum1D] = (peak_flux[spectrum1D] *
                                  (nu[spectrum1D] * inu_m[spectrum1D]) ** (1. / 3.))

            output[spectrum1G] = (peak_flux[spectrum1G] *
                                  (nu[spectrum1G] * inu_m[spectrum1G]) ** bp)

            # spectrum 2 output
            output[spectrum2B] = (peak_flux[spectrum2B] *
                                  (nu_a[spectrum2B] * inu_m[spectrum2B]) ** (1. / 3.) *
                                  (nu[spectrum2B] * inu_a[spectrum2B]) ** (2.))

            output[spectrum2A] = (peak_flux[spectrum2A] *
                                  (nu_a[spectrum2A] * inu_m[spectrum2A]) ** (-5. / 3.) *
                                  (nu[spectrum2A] * inu_m[spectrum2A]) ** (5. / 2.))

            output[spectrum2G] = (peak_flux[spectrum2G] *
                                  (nu[spectrum2G] * inu_m[spectrum2G]) ** bp)

        return output


################################################################################

class Data:

    def __init__(self):

        t0 = None
        nu0 = None
        t1 = None
        nu1 = None
        F = None
        DF = None
        plotgroup = None  # useful for simultaneously plotting multiple light curves
        # from a broadband data set

    # -----------------------------------------------------------------------------

    def set_minimum_F(self, Fmin=0.):
        ######################################################################################## add commentary

        if hasattr(Fmin, "__len__"):

            for it in range(len(self.t0)):
                self.F[it] = np.maximum(Fmin[it], self.F[it])

        else:

            for it in range(len(self.t0)):
                self.F[it] = np.maximum(Fmin, self.F[it])

    # -----------------------------------------------------------------------------

    def perturb(self, Fmin=1e-5):
        ########################################################################################## add commentary

        Fminlist = Fmin * self.F
        self.F = self.F + self.DF * np.random.randn(len(self.F))
        self.set_minimum_F(Fmin=Fminlist)

    # -----------------------------------------------------------------------------

    def __len__(self):

        # Overload len default method to return number of data points in data set.

        return len(self.t0)


################################################################################

class SF_F:

    def __init__(self):

        self.Fnusource = None  # pointer to function providing monochromatic flux
        self.flux = None  # pointer to the fit function

        self.nu_stencil = 5
        self.t_stencil = 5

    # -----------------------------------------------------------------------------

    def Fnu(self, params, dataset=None, t0=None, nu0=None):

        if t0 is None:
            t0 = dataset.t0

        if nu0 is None:
            nu0 = dataset.nu0

        return self.Fnusource.Fnu(params, t0, nu0)

    # -----------------------------------------------------------------------------

    def F(self, params, dataset=None, t0=None, nu0=None, nu1=None):
        """
        Compute frequency-integrated flux, using as many frequencies as listed in
        freqs for each requested time. For now, the integration is a simple
        linear approximation between points. We integrate in this simple manner
        rather than hard-coding a spectral shape to allow for (1) alternative
        radiation mechanisms with a shape that we cannot anticipate here
        (including smoothened spectra), and (2) extra actions on the spectra
        such as dust extinction and X-ray absorption.

        Returns:
          ndarray: frequency-integrated result
        """

        if t0 is None:
            t0 = dataset.t0

        if nu0 is None:
            nu0 = dataset.nu0

        if nu1 is None:
            nu1 = dataset.nu1

        N = len(t0)
        t_expand = np.empty([N, self.nu_stencil])
        t_expand[:] = t0[:, None]

        f = np.arange(self.nu_stencil)
        lognu0 = np.log10(nu0)
        lognu1 = np.log10(nu1)
        dlognu = (lognu1 - lognu0) / self.nu_stencil

        nu_expand = np.empty([N, self.nu_stencil])
        nu_expand[:, :] = nu0[:, None] * 10. ** ((f[None, :] + 0.5) * dlognu[:, None])

        dnu_expand = np.empty([N, self.nu_stencil])
        dnu_expand[:, :] = (nu0[:, None] *
                            (10. ** ((f[None, :] + 1) * dlognu[:, None]) -
                             10. ** (f[None, :] * dlognu[:, None])))

        output_Fnu = self.Fnusource.Fnu(params, t_expand.flatten(),
                                        nu_expand.flatten()).reshape((N, self.nu_stencil))

        output_F = (output_Fnu * dnu_expand).sum(axis=1)

        return output_F

    # -----------------------------------------------------------------------------

    def fluence(self, params, dataset=None, t0=None, t1=None, nu0=None, nu1=None):
        """
        Returns fluence

        Args:
          t (ndarray): Observer times (seconds), 2D array, where the second
            dimension has two entries for beginning and end time of each time bin

          nu (ndarrays): frequencies, provided in the form of a list of
            1D-arrays, where the entries in each array are the frequencies at that
            particular observation time.

          Dnu (list of ndarrays): bin widths corresponding to the frequencies nu

          params (ndarray): model parameters for which to compute fluence

        Returns:
          ndarray: array of fluences for the time bins indicated with t. The
            frequencies at each time entry have been summed over

        """

        if t0 is None:
            t0 = dataset.t0

        if t1 is None:
            t1 = dataset.t1

        if nu0 is None:
            nu0 = dataset.nu0

        if nu1 is None:
            nu1 = dataset.nu1

        N = len(t0)
        nu0_expand = np.empty([N, t_stencil])
        nu0_expand[:] = nu0[:, None]

        nu1_expand = np.empty([N, t_stencil])
        nu1_expand[:] = nu1[:, None]

        f = np.arange(t_stencil)
        logt0 = np.log10(t0)
        logt1 = np.log10(t1)
        dlogt = (logt1 - logt0) / t_stencil

        t_expand = np.empty([N, t_stencil])
        t_expand[:, :] = t0[:, None] * 10. ** ((f[None, :] + 0.5) * dlogt[:, None])

        dt_expand = np.empty([N, t_stencil])
        dt_expand[:, :] = (t0[:, None] *
                           (10. ** ((f[None, :] + 1) * dlogt[:, None]) -
                            10. ** (f[None, :] * dlogt[:, None])))

        output_F = self.F(params, t0=t_expand.flatten(), nu0=nu0_expand.flatten(),
                          nu1=nu1_expand.flatten()).reshape((N, t_stencil))

        output_fluence = (outputF * dt_expand).sum(axis=1)

        return output_fluence

    # -----------------------------------------------------------------------------

    def fluence_over_dt(self, dataset, params):
        """
        Returns fluence divided by temporal bin width
        """

        # STILL TO DO
