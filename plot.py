import george
import grb_tools.boxfit as boxfit
import grb_tools.file_reader as file_reader
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import pandas as pn
from george import kernels

import misc

mpl.use('Qt4Agg', warn=False, force=True)
mpl.style.use('seaborn-dark-palette')
# mpl.style.use('grayscale')
mpl.rcParams['text.usetex'] = True
mpl.rcParams['text.latex.preamble'] = [r'\usepackage{amsmath}']  # for \text command
params = {'legend.fontsize': 6,
          'legend.handlelength': 2}
plt.rcParams.update(params)


def read_obs_data(data_filename, accuracy=0.03):
    df = pn.read_csv(data_filename, delimiter=',', comment='#', header=None, names=['Time', 'Freq', 'Flux', 'Error'])
    bands = misc.group(df['Freq'].unique(), accuracy=accuracy)
    return df, bands


def plot_obs_data(data_filename, accuracy=0.03, panels=[1e8, 1e12, 1e14, 1e16, 1e19], factor_val=1, figsize=(10, 8),
                  markersize=3):
    df_data, bands = read_obs_data(data_filename, accuracy=accuracy)
    plt_per_panel = np.zeros(len(panels) - 1)
    for i in range(len(plt_per_panel)):
        for j, band in enumerate(bands):
            if panels[i] <= band < panels[i + 1]:
                plt_per_panel[i] = plt_per_panel[i] + 1
    no_panels = 0
    i_panels = []
    for i in range(len(plt_per_panel)):
        if plt_per_panel[i] > 0:
            no_panels = no_panels + 1
            i_panels.append(i)
    fig, axes = plt.subplots(no_panels, 1, sharex=True, figsize=figsize)
    for i in range(no_panels):
        i_factor = 0
        for j, band in enumerate(bands):
            if panels[i_panels[i]] <= band < panels[i_panels[i] + 1]:
                color = next(axes[i]._get_lines.prop_cycler)['color']
                mask = misc.get_mask(df_data, band, accuracy=accuracy)
                mask_det = misc.get_detection_mask(df_data)
                mask_up = misc.get_upperlimit_mask(df_data)
                factor = factor_val ** i_factor
                time_det = df_data['Time'][mask & mask_det].values / 86400
                flux_det = df_data['Flux'][mask & mask_det].values * factor
                error_det = df_data['Error'][mask & mask_det].values * factor
                time_up = df_data['Time'][mask & mask_up].values / 86400
                flux_up = df_data['Flux'][mask & mask_up].values * factor
                error_up = df_data['Error'][mask & mask_up].values
                if factor == 1:
                    label = r'{:.2E} Hz'.format(band)
                else:
                    label = r'{:.2E} Hz $(\times {:d})$'.format(band, factor)
                axes[i].errorbar(time_det, flux_det, yerr=error_det, fmt='o', markersize=markersize,
                                 label=label)
                axes[i].errorbar(time_up, flux_up + 3 * error_up, fmt='v', markersize=markersize, label=None,
                                 color=color)
                axes[i].set_xscale('log')
                axes[i].set_yscale('log')
                axes[i].set_ylabel(r'$f_\nu$ (mJy)')
                axes[i].legend(loc=1)
                i_factor = i_factor + 1
    plt.xlabel('Time (days)')


def plot_scalefit(data_filename, scalefit_object, scalefit_parameters, accuracy=0.03,
                  panels=[1e8, 1e12, 1e14, 1e16, 1e19], factor_val=1, model_t_margin=0.2, figsize=(10, 8), markersize=3,
                  no_points=100, persistent_bands=None, persistent_parameters=None, log10_flux=False):
    df_data, bands = read_obs_data(data_filename, accuracy=accuracy)
    plt_per_panel = np.zeros(len(panels) - 1)
    for i in range(len(plt_per_panel)):
        for j, band in enumerate(bands):
            if panels[i] <= band < panels[i + 1]:
                plt_per_panel[i] = plt_per_panel[i] + 1
    no_panels = 0
    i_panels = []
    for i in range(len(plt_per_panel)):
        if plt_per_panel[i] > 0:
            no_panels = no_panels + 1
            i_panels.append(i)
    axes = []
    fig, _axes = plt.subplots(no_panels, 1, sharex=True, figsize=figsize)
    if no_panels == 1:
        axes.append(_axes)
    else:
        axes = _axes
    for i in range(no_panels):
        i_factor = 0
        for j, band in enumerate(bands):
            band_arr = np.full(no_points, band)
            if panels[i_panels[i]] <= band < panels[i_panels[i] + 1]:
                color = next(axes[i]._get_lines.prop_cycler)['color']
                mask = misc.get_mask(df_data, band, accuracy=accuracy)
                mask_det = misc.get_detection_mask(df_data)
                mask_up = misc.get_upperlimit_mask(df_data)
                factor = factor_val ** i_factor
                time_det = df_data['Time'][mask & mask_det].values / 86400
                flux_det = df_data['Flux'][mask & mask_det].values * factor
                error_det = df_data['Error'][mask & mask_det].values * factor
                time_up = df_data['Time'][mask & mask_up].values / 86400
                flux_up = df_data['Flux'][mask & mask_up].values
                error_up = df_data['Error'][mask & mask_up].values
                three_sigma_up = (flux_up + 3 * error_up) * factor

                time_model = np.logspace(np.log10(min(df_data['Time'][mask].values) * (1 - model_t_margin)),
                                         np.log10(max(df_data['Time'][mask].values) * (1 + model_t_margin)), no_points)
                if persistent_bands is None:
                    flux_model = scalefit_object.Fnu(scalefit_parameters, time_model, band_arr) * factor
                else:
                    flux_model = scalefit_object.Fnu(scalefit_parameters, time_model, band_arr)
                    for b, p_band in enumerate(persistent_bands):
                        if misc.in_range(band, p_band):
                            if log10_flux:
                                flux_model = flux_model + 10 ** persistent_parameters[b]
                            else:
                                flux_model = flux_model + persistent_parameters[b]
                    flux_model = flux_model * factor
                time_model = time_model / 86400
                if factor == 1:
                    axes[i].errorbar(time_det, flux_det, yerr=error_det, fmt='o', markersize=markersize,
                                     label=r'{:.2E} Hz'.format(band), color=color)
                else:
                    axes[i].errorbar(time_det, flux_det, yerr=error_det, fmt='o', markersize=markersize,
                                     label=r'{:.2E} Hz $(\times {:d})$'.format(band, factor), color=color)

                axes[i].errorbar(time_up, three_sigma_up, fmt='v', markersize=markersize, label=None, color=color)
                axes[i].plot(time_model, flux_model, ls='-', label=None, color=color)

                axes[i].set_xscale('log')
                axes[i].set_yscale('log')
                axes[i].set_ylabel(r'$f_\nu$ (mJy)')
                axes[i].legend(loc=1)
                i_factor = i_factor + 1
        plt.xlabel('Time (days)')


def plot_scalefit_multiple(data_filename, scalefit_object, scalefit_parameters, accuracy=0.03,
                           panels=[1e8, 1e12, 1e14, 1e16, 1e19], factor_val=1, model_t_margin=0.2, figsize=(10, 8),
                           markersize=3,
                           no_points=100, persistent_bands=None, persistent_parameters=None, log10_flux=False,
                           alpha=0.5,
                           grb_name=''):
    n_sample = len(scalefit_parameters)
    df_data, bands = read_obs_data(data_filename, accuracy=accuracy)
    plt_per_panel = np.zeros(len(panels) - 1)
    for i in range(len(plt_per_panel)):
        for j, band in enumerate(bands):
            if panels[i] <= band < panels[i + 1]:
                plt_per_panel[i] = plt_per_panel[i] + 1
    no_panels = 0
    i_panels = []
    for i in range(len(plt_per_panel)):
        if plt_per_panel[i] > 0:
            no_panels = no_panels + 1
            i_panels.append(i)
    axes = []
    fig, _axes = plt.subplots(no_panels, 1, sharex=True, figsize=figsize)
    _axes[0].set_title(grb_name)
    if no_panels == 1:
        axes.append(_axes)
    else:
        axes = _axes
    for i in range(no_panels):
        i_factor = 0
        for j, band in enumerate(bands):
            band_arr = np.full(no_points, band)
            if panels[i_panels[i]] <= band < panels[i_panels[i] + 1]:
                color = next(axes[i]._get_lines.prop_cycler)['color']
                mask = misc.get_mask(df_data, band, accuracy=accuracy)
                mask_det = misc.get_detection_mask(df_data)
                mask_up = misc.get_upperlimit_mask(df_data)
                factor = factor_val ** i_factor
                time_det = df_data['Time'][mask & mask_det].values / 86400
                flux_det = df_data['Flux'][mask & mask_det].values * factor
                error_det = df_data['Error'][mask & mask_det].values * factor
                time_up = df_data['Time'][mask & mask_up].values / 86400
                flux_up = df_data['Flux'][mask & mask_up].values
                error_up = df_data['Error'][mask & mask_up].values
                three_sigma_up = (flux_up + 3 * error_up) * factor

                time_model = np.logspace(np.log10(min(df_data['Time'][mask].values) * (1 - model_t_margin)),
                                         np.log10(max(df_data['Time'][mask].values) * (1 + model_t_margin)), no_points)
                flux_model_range = np.zeros((n_sample, len(time_model)))
                flux_model_range_max = np.zeros(len(time_model))
                flux_model_range_min = np.zeros(len(time_model))
                if persistent_bands is None:
                    for k in range(n_sample):
                        flux_model_range[k] = scalefit_object.Fnu(scalefit_parameters[k], time_model, band_arr) * factor
                    for k in range(len(time_model)):
                        flux_model_range_min[k] = min(flux_model_range[:, k])
                        flux_model_range_max[k] = max(flux_model_range[:, k])
                else:
                    for k in range(n_sample):
                        flux_model_range[k] = scalefit_object.Fnu(scalefit_parameters[k], time_model, band_arr)
                        for b, p_band in enumerate(persistent_bands):
                            if misc.in_range(band, p_band):
                                if log10_flux:
                                    flux_model_range[k] = flux_model_range[k] + 10 ** persistent_parameters[k][b]
                                else:
                                    flux_model_range[k] = flux_model_range[k] + persistent_parameters[k][b]
                        flux_model_range[k] = flux_model_range[k] * factor
                    for k in range(len(time_model)):
                        flux_model_range_min[k] = min(flux_model_range[:, k])
                        flux_model_range_max[k] = max(flux_model_range[:, k])
                time_model = time_model / 86400
                if factor == 1:
                    axes[i].errorbar(time_det, flux_det, yerr=error_det, fmt='o', markersize=markersize,
                                     label=r'{:.2E} Hz'.format(band), color=color)
                else:
                    axes[i].errorbar(time_det, flux_det, yerr=error_det, fmt='o', markersize=markersize,
                                     label=r'{:.2E} Hz $(\times {:d})$'.format(band, factor), color=color)

                axes[i].errorbar(time_up, three_sigma_up, fmt='v', markersize=markersize, label=None, color=color)
                for k in range(n_sample):
                    axes[i].plot(time_model, flux_model_range[k], ls='-', label=None, color=color, alpha=0.05)
                # axes[i].fill_between(time_model, flux_model_range_min, flux_model_range_max, color=color, alpha=alpha,
                #                      linewidth=0.0)

                axes[i].set_xscale('log')
                axes[i].set_yscale('log')
                axes[i].set_ylabel(r'$f_\nu$ (mJy)')
                axes[i].legend(loc=1)
                i_factor = i_factor + 1
        plt.xlabel('Time (days)')


def plot_scalefit_gp(data_filename, scalefit_object, scalefit_parameters, hyper_parameters, fio_object, accuracy=0.03,
                     panels=[1e8, 1e12, 1e14, 1e16, 1e19], factor_val=1, model_t_margin=0.2, figsize=(10, 8),
                     markersize=3, no_points=100, persistent_bands=None, persistent_parameters=None, log10_flux=False,
                     plt_boxfit=False, box_lc_output_prefix='box_lc_'):
    if plt_boxfit:
        if scalefit_parameters[scalefit_object.index_p_] < 2.0:
            plt_boxfit = False
    df_data, bands = read_obs_data(data_filename, accuracy=accuracy)
    plt_per_panel = np.zeros(len(panels) - 1)
    for i in range(len(plt_per_panel)):
        for j, band in enumerate(bands):
            if panels[i] <= band < panels[i + 1]:
                plt_per_panel[i] = plt_per_panel[i] + 1
    no_panels = 0
    i_panels = []
    for i in range(len(plt_per_panel)):
        if plt_per_panel[i] > 0:
            no_panels = no_panels + 1
            i_panels.append(i)
    axes = []
    fig, _axes = plt.subplots(no_panels, 1, sharex=True, figsize=figsize)
    if no_panels == 1:
        axes.append(_axes)
    else:
        axes = _axes
    for i in range(no_panels):
        i_factor = 0
        for j, band in enumerate(bands):
            band_arr = np.full(no_points, band)
            if panels[i_panels[i]] <= band < panels[i_panels[i] + 1]:
                color = next(axes[i]._get_lines.prop_cycler)['color']
                mask = misc.get_mask(df_data, band, accuracy=accuracy)
                mask_det = misc.get_detection_mask(df_data)
                mask_up = misc.get_upperlimit_mask(df_data)
                factor = factor_val ** i_factor
                time_det = df_data['Time'][mask & mask_det].values / 86400
                flux_det = df_data['Flux'][mask & mask_det].values * factor
                error_det = df_data['Error'][mask & mask_det].values * factor
                time_up = df_data['Time'][mask & mask_up].values / 86400
                flux_up = df_data['Flux'][mask & mask_up].values
                error_up = df_data['Error'][mask & mask_up].values
                three_sigma_up = (flux_up + 3 * error_up) * factor

                a1, a2, l1, l2, white_noise = hyper_parameters
                k = 10 ** a1 * kernels.ExpSquaredKernel([10 ** l1, 10 ** l2], ndim=2)
                # k1 = 10 ** a1 * kernels.ExpSquaredKernel(10 ** l1, axes=0, ndim=2)
                # k2 = 10 ** a2 * kernels.ExpSquaredKernel(10 ** l2, axes=1, ndim=2)
                # k = k1 # + k2
                # gp = george.GP(k, white_noise=white_noise)
                gp = george.GP(k)
                if fio_object.conf_data['fit']['gp']['log_time_coord']:
                    x1 = fio_object.log_obs_time
                else:
                    x1 = fio_object.obs_time
                if fio_object.conf_data['fit']['gp']['log_freq_coord']:
                    x2 = fio_object.log_obs_freq
                else:
                    x2 = fio_object.obs_freq
                x_2d_gp = np.column_stack((x1, x2))
                if fio_object.conf_data['fit']['gp']['use_errorbars']:
                    if fio_object.conf_data['fit']['gp']['log_flux']:
                        gp.compute(x_2d_gp, fio_object.log_obs_error * np.exp(white_noise))
                    else:
                        gp.compute(x_2d_gp, fio_object.obs_error * np.exp(white_noise))
                else:
                    gp.compute(x_2d_gp)
                f_nu_gp = scalefit_object.Fnu(scalefit_parameters, fio_object.obs_time, fio_object.obs_freq)
                if persistent_bands is not None:
                    for b, p_band in enumerate(persistent_bands):
                        if misc.in_range(band, p_band):
                            if log10_flux:
                                f_nu_gp = f_nu_gp + 10 ** persistent_parameters[b]
                            else:
                                f_nu_gp = f_nu_gp + persistent_parameters[b]
                if fio_object.conf_data['fit']['gp']['log_flux']:
                    y = fio_object.log_obs_flux - np.log(f_nu_gp)
                else:
                    y = fio_object.obs_flux - f_nu_gp

                time_model = np.logspace(np.log10(min(df_data['Time'][mask].values) * (1 - model_t_margin)),
                                         np.log10(max(df_data['Time'][mask].values) * (1 + model_t_margin)),
                                         no_points)
                freq_model = np.full(len(time_model), band)
                if fio_object.conf_data['fit']['gp']['log_time_coord']:
                    x1 = np.log(time_model)
                else:
                    x1 = time_model
                if fio_object.conf_data['fit']['gp']['log_freq_coord']:
                    x2 = np.log(freq_model)
                else:
                    x2 = freq_model
                x_2d = np.column_stack((x1, x2))
                mean_gp, cov = gp.predict(y, x_2d)
                sigma = np.sqrt(np.diag(cov))
                if plt_boxfit:
                    # index entry numbers, for clarity
                    # index_z_ = 0
                    # index_log10_d_L_28_ = 1
                    # index_log10_E_iso_53_ = 2
                    # index_log10_n_ref_ = 3
                    # index_theta_0_ = 4
                    # index_theta_obs_over_theta_0_ = 5
                    # index_p_ = 6
                    # index_log10_eps_e_bar_ = 7
                    # index_log10_eps_B_ = 8
                    # index_log10_xi_N_ = 9
                    boxfit_parameters = np.zeros(8)
                    boxfit_parameters[0] = scalefit_parameters[scalefit_object.index_theta_0_]
                    boxfit_parameters[1] = 10 ** scalefit_parameters[scalefit_object.index_log10_E_iso_53_] * 1e53
                    boxfit_parameters[2] = 10 ** scalefit_parameters[scalefit_object.index_log10_n_ref_]
                    boxfit_parameters[3] = scalefit_parameters[scalefit_object.index_theta_obs_over_theta_0_] * \
                                           boxfit_parameters[0]
                    boxfit_parameters[4] = scalefit_parameters[scalefit_object.index_p_]
                    boxfit_parameters[5] = 10 ** scalefit_parameters[scalefit_object.index_log10_eps_B_]
                    boxfit_parameters[6] = 10 ** scalefit_parameters[scalefit_object.index_log10_eps_e_bar_] * (
                            boxfit_parameters[4] - 1) / (boxfit_parameters[4] - 2)
                    boxfit_parameters[7] = 10 ** scalefit_parameters[scalefit_object.index_log10_xi_N_]
                    boxfit.generate_light_curve(boxfit_parameters,
                                                min(df_data['Time'][mask].values) * (1 - model_t_margin),
                                                max(df_data['Time'][mask].values) * (1 + model_t_margin), band,
                                                no_points=no_points, z=scalefit_parameters[scalefit_object.index_z_],
                                                d_l=10 ** scalefit_parameters[
                                                    scalefit_object.index_log10_d_L_28_] * 1e28,
                                                output_filename=box_lc_output_prefix + '{0}.txt'.format(j))
                    df_box = file_reader.read_boxfit_output(box_lc_output_prefix + '{0}.txt'.format(j),
                                                            accuracy=accuracy)
                    box_time = df_box['Time'].values / 86400
                    box_flux = df_box['Flux'].values
                # print(mean_gp, sigma, band)
                if persistent_bands is None:
                    flux_model = scalefit_object.Fnu(scalefit_parameters, time_model, band_arr) * factor
                    if plt_boxfit:
                        box_flux = box_flux * factor
                else:
                    flux_model = scalefit_object.Fnu(scalefit_parameters, time_model, band_arr)
                    for b, p_band in enumerate(persistent_bands):
                        if misc.in_range(band, p_band):
                            if log10_flux:
                                flux_model = flux_model + 10 ** persistent_parameters[b]
                                if plt_boxfit:
                                    box_flux = box_flux + 10 ** persistent_parameters[b]
                            else:
                                flux_model = flux_model + persistent_parameters[b]
                                if plt_boxfit:
                                    box_flux = box_flux + persistent_parameters[b]
                    flux_model = flux_model * factor
                    if plt_boxfit:
                        box_flux = box_flux * factor
                if fio_object.conf_data['fit']['gp']['log_flux']:
                    mean_model = np.exp(np.log(flux_model) + mean_gp)
                    sigma_up_model = np.exp(np.log(flux_model) + mean_gp + sigma)
                    sigma_low_model = np.exp(np.log(flux_model) + mean_gp - sigma)
                else:
                    mean_model = flux_model + mean_gp
                    sigma_up_model = flux_model + mean_gp + sigma
                    sigma_low_model = flux_model + mean_gp - sigma
                time_model = time_model / 86400
                if factor == 1:
                    axes[i].errorbar(time_det, flux_det, yerr=error_det, fmt='o', markersize=markersize,
                                     label=r'{:.2E} Hz'.format(band), color=color)
                else:
                    axes[i].errorbar(time_det, flux_det, yerr=error_det, fmt='o', markersize=markersize,
                                     label=r'{:.2E} Hz $(\times {:d})$'.format(band, factor), color=color)

                axes[i].errorbar(time_up, three_sigma_up, fmt='v', markersize=markersize, label=None, color=color)
                axes[i].plot(time_model, mean_model, ls='--', label=None, color=color)
                axes[i].fill_between(time_model, sigma_low_model, sigma_up_model,
                                     facecolor=color, alpha=0.2)
                axes[i].plot(time_model, flux_model, ls='-', label=None, color=color)
                if plt_boxfit:
                    axes[i].plot(box_time, box_flux, ls='-.', label=None,
                                 color=color)

                axes[i].set_xscale('log')
                axes[i].set_yscale('log')
                axes[i].set_ylabel(r'$f_\nu$ (mJy)')
                axes[i].legend(loc=1)
                i_factor = i_factor + 1
        plt.xlabel('Time (days)')
